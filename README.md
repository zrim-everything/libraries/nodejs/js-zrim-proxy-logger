# Zrim Proxy Logger

```javascript
require('zrim-proxy-logger')
```

## Introduction

Sometime you need to ensure to have a logger even without a target at the end to 
ensure you code will always work and have the logger set.

This require to be able to dynamically change the logger in object already created, which
complicate the development.

A proxy allow you to have a defined logger and be able to change the target logger in
a central place

Please see [documentations](documentations/README.md)
