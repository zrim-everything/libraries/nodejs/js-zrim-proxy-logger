# Using Proxy Logger

## Register a Manager

In case you would use your own logger manager or the latest version, you
need to force the system to use you manager.

Create your manager and register it
```javascript
// Or you manager
const SimpleLoggerManagerV1 = require('zrim-proxy-logger').services.SimpleLoggerManagerV1;
const _ = require('lodash');
const manager = new SimpleLoggerManagerV1();

// Register with the global variable so any different version will reach it
_.set(global, 'eu.zrimeverything.proxyLogger.defaultLoggerManager', manager);
```

At this moment the system will use this manager to fetch new logger.


This step is just mandatory, however, you must ensure all module use the same
version pf `zrim-proxy-logger`.

## Configure your target logger

It is recommended to register you target the earliest possible moment to not loose any log.

You can configure by:
- Calling your manager directly. This has the effect of having you code close to your manager
- Calling the handler with the `setLogger` method. This is more generic but you loose possible extra
argument you'd like to pass to your manager.

## Create your logger

Example to create logger to all your object
```javascript
const defaultLoggerManagerHandler = require('zrim-proxy-logger').defaultLoggerManagerHandler;

class MyClass {
  constructor() {
    this.logger = defaultLoggerManagerHandler.getLogger('MyClass');
  }
}

const a = new MyClass();
a.logger.debug("My Debug");
```

## Update the target

You can update the target.

Like a special target for all logger `MyClass`. And a default one.

So `a.logger.debug()` will go to your new target.

