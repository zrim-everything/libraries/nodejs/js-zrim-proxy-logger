# Base Logger Proxy

```javascript
require('zrim-proxy-logger').BaseProxyLogger;
```

## Introduction

Base class to define a proxy logger.

The class contains the generic implementation of a logger.

The proxy logger allow to have a parent logger to merge it information or use parent target.
