# Services

```javascript
require('zrim-proxy-logger').services
```

## Introduction

Contains predefined services for the proxy logger system.

## Specifications

See [specs](logger-manager-specs.md)

## Services

- [SimpleLoggerManagerV1](simple-logger-manager-v1.md)
