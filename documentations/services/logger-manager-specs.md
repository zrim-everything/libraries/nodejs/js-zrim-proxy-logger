# Logger Manager : Specification

A logger manager aims to manage the logger given to each caller.
It must keep always provide a logger to any call given.

## All version

All manager must provide :

Methods:
- getVersion() : string : Returns the semantic version of the manager. The major version
correspond to the specification version which implement

## Version 1

### Logger

A logger must provide the minimum method:
- log(levelName: string, ...args: any[]);

### Interface

A logger manager must provide:
- Methods
  - getLogger(name?: string): Logger
    - Returns a logger. Can be any implantation
  - setTargetLogger(name?: string, target?: TargetLogger): void
    - Set the target
  - getTargetLogger(name?: string): TargetLogger | undefined
  - listLevelNames() : string[]
    - List currently known logger names 
  - listTargetLoggerNames() : string[]
    - List currently known target names 
