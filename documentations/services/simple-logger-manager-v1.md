# Simple Logger Manager (spec v1)

```javascript
require('zrim-proxy-logger').services.SimpleLoggerManagerV1
```

## Introduction

Implement the [specification v1](logger-manager-specs.md).

The manager returns always a [proxy logger](../base-proxy-logger.md) allowing to dynamically
change the target after the logger has been created.


