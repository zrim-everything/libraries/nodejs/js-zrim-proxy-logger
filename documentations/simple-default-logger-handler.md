# Default Logger Handler

```javascript
require('zrim-proxy-logger').SimpleDefaultLoggerHandler
```

## Introduction

Simple class to manage the logger manager. It is a high level wrapper.
It is proxy pass any call to the right manager.

Any call made to set or get a logger, the instance will try to find the right manager defined 
by the user.

This allow the user to define the logger even after the instance has been created.

## Lookup

The by-pass node different version of the package, the implementation look into the global
variable for the path:
- eu.zrimeverything.proxyLogger.defaultLoggerManager
- eu.zrimeverything.core.defaultLoggerManager
- jsZrimCore.defaultLoggerManager

If all of the path does not provide a manager, it will use the default one.

The manager should not share logger, so you must ensure to set the manager to use before creating
any logger.

## Versions

The handler will ask the manager with the method `getVersion` to know the specification
implementation used, and redirect calls to the methods
- _getLoggerV\<version\>
- _getTargetV\<version\>
- _setTargetV\<version\>
- _listLoggerNamesV\<version\>
- _listTargetLoggerNamesV\<version\>

This allow end user to add their own implementation with a different specification version.
