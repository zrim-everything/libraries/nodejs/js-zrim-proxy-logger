import {ProxyLogger} from "./proxy-logger";
import {targetLogger, TargetLogger} from "./target-logger";
import * as _ from "lodash";
import Debug = require("debug");
import {SyslogLogger} from "./logger";
import {GenericLogger, genericLogger} from "./generic-logger";

export namespace baseProxyLogger {

  export interface Properties {
    /**
     * The target logger
     */
    target?: TargetLogger | undefined;

    /**
     * Meta data
     */
    metaData?: genericLogger.LoggerMetaData;

    /**
     * Parent logger
     */
    parent?: GenericLogger | undefined;

    /**
     * Contains prefixes
     */
    prefixes: string[];
  }

  export interface ConstructorOptions {
    /**
     * The target logger
     */
    target?: TargetLogger | undefined;

    /**
     * Meta data
     */
    metaData?: genericLogger.LoggerMetaData;

    /**
     * Parent logger
     */
    parent?: GenericLogger | undefined;

    /**
     * Contains prefixes
     */
    prefixes?: string[];
  }

  export interface HandleLogOptions {
    /**
     * The origin instance where the log come from
     */
    origin: BaseProxyLogger;

    /**
     * The targets found to use
     */
    targets: TargetLogger[];

    /**
     * The meta data
     */
    metaData?: object;

    /**
     * The level name
     */
    levelName: string;

    /**
     * The raw user arguments not modified
     */
    rawUserArguments: any[];

    /**
     * The user arguments
     */
    userArguments: any[];
  }
}

function mergeWithArray(objValue, srcValue) {
  if (_.isArray(objValue)) {
    return _.concat(objValue, srcValue);
  }
}

const debug = Debug('ze:proxy-logger:base');

/**
 * Base proxy logger implementation
 */
export class BaseProxyLogger implements ProxyLogger, SyslogLogger {

  /**
   * Internal properties
   */
  protected properties: baseProxyLogger.Properties;

  constructor(options?: baseProxyLogger.ConstructorOptions) {
    Object.defineProperty(this, 'properties', {
      value: {
        target: undefined,
        prefixes: [],
        metaData: undefined,
        parent: undefined
      },
      enumerable: false,
      configurable: true
    });

    this._initialize(options);
  }

  /**
   * @inheritDoc
   */
  public get parent(): GenericLogger {
    return this.properties.parent;
  }

  /**
   * @inheritDoc
   */
  public set parent(value: GenericLogger | undefined) {
    this.properties.parent = value || undefined;
  }

  /**
   * @inheritDoc
   */
  public get target(): TargetLogger | undefined {
    return this.properties.target;
  }

  /**
   * @inheritDoc
   */
  public set target(value: TargetLogger | undefined) {
    this.properties.target = value || undefined;
  }

  /**
   * @inheritDoc
   */
  public get prefixes(): string[] {
    return this.properties.prefixes;
  }

  /**
   * @inheritDoc
   */
  public set prefixes(value: string[]) {
    this.properties.prefixes = value.filter(v => v.length > 0);
  }

  /**
   * @inheritDoc
   */
  public get metaData(): genericLogger.LoggerMetaData {
    return this.properties.metaData;
  }

  /**
   * @inheritDoc
   */
  public set metaData(value: genericLogger.LoggerMetaData) {
    this.properties.metaData = value;
  }

  /**
   * @inheritDoc
   */
  public isVerboseEnabled(): boolean {
    const target = this._selectTargets()[0];
    return target ? target.isVerboseEnabled() : false;
  }

  /**
   * @inheritDoc
   */
  public serializeMetaData(options: genericLogger.SerializeMetaDataOptions): object | undefined {
    try {
      let parentMetaData;

      // Check to know if there is a parent
      if (this.properties.parent) {
        parentMetaData = this.properties.parent.serializeMetaData(options);
      }

      const metaDataType = typeof this.properties.metaData;
      let metaDataGenerated;
      switch (metaDataType) {
        case 'function':
          metaDataGenerated = (this.properties.metaData as genericLogger.LoggerMetaDataGenerator)(this, options);
          break;
        case 'object':
          metaDataGenerated = this.properties.metaData;
          break;
        case 'string':
        case 'number':
        case 'boolean':
          metaDataGenerated = {
            data: this.properties.metaData
          };
          break;
        default:
          break;
      }

      if (parentMetaData || metaDataGenerated || this.properties.prefixes.length > 0) {
        const metaData = {};

        if (this.properties.prefixes.length > 0) {
          _.mergeWith(metaData, {
            loggerPrefixes: this.properties.prefixes
          }, mergeWithArray);
        }

        _.mergeWith(metaData, parentMetaData, mergeWithArray);
        _.mergeWith(metaData, metaDataGenerated, mergeWithArray);

        return metaData;
      }
    } catch (unexpectedError) {
      // To be sure to tell someone
      return {
        __Logger_unexpectedError: {
          error: unexpectedError,
          stack: unexpectedError.stack,
          message: unexpectedError.message
        }
      };
    }
  }

  /**
   * @inheritDoc
   */
  public of(options?: genericLogger.OfOptions): ProxyLogger {
    const constructorArguments = [{
      parent: this,
      target: options.target
    }];

    this._configureNewInstanceArguments(constructorArguments, options);

    // Create the new instance
    const instance = Reflect.construct(this.constructor, constructorArguments);
    return instance;
  }

  /**
   * @inheritDoc
   */
  public log(level: string, ...args: any[]): void;
  /**
   * @inheritDoc
   */
  public log(level: string, metaData: object | undefined, ...args: any[]): void;
  /**
   * @inheritDoc
   */
  public log(level: string, ...args: Array<any | object | undefined>): void {
    if (typeof level !== 'string') {
      debug("Received invalid level name type");
      return;
    }

    const targets = this._selectTargets();
    if (targets.length === 0) {
      debug("No target available");
      return;
    }

    const rawUserArgs = args.concat();
    const userArgs = [];

    // Find the input meta data
    const userInputMetaData = {};

    // Check if the user give meta data
    if (_.isObjectLike(args[0])) {
      const obj = args.splice(0, 1)[0];
      if (obj instanceof Error) {
        // No simple error
        userInputMetaData['error'] = obj;
      } else {
        // We have metadata
        _.merge(userInputMetaData, obj);
      }
    }

    const lastValueIndex = args.length - 1;
    if (lastValueIndex >= 0 && _.isObjectLike(args[lastValueIndex])) {
      const obj = args.splice(lastValueIndex, 1)[0];

      if (obj instanceof Error) {
        // This is not a simple object but an error
        userInputMetaData['error'] = obj;
      } else {
        _.mergeWith(userInputMetaData, obj, mergeWithArray);
      }
    }

    userArgs.push(...args);
    const generatedMetaData = this.serializeMetaData({
      userArguments: userArgs
    });

    const metaData = {};
    _.mergeWith(metaData, generatedMetaData, mergeWithArray);
    _.mergeWith(metaData, userInputMetaData, mergeWithArray);

    this._handleLog({
      origin: this,
      targets,
      levelName: level,
      metaData,
      userArguments: userArgs,
      rawUserArguments: rawUserArgs
    });
  }

  /**
   * @inheritDoc
   */
  public crit(...args: any[]): void {
    this.log('crit', ...args);
  }

  /**
   * @inheritDoc
   */
  public debug(...args: any[]): void {
    this.log('debug', ...args);
  }

  /**
   * @inheritDoc
   */
  public silly(...args: any[]): void {
    this.debug(...args);
  }

  /**
   * @inheritDoc
   */
  public error(...args: any[]): void {
    this.log('error', ...args);
  }

  /**
   * @inheritDoc
   */
  public info(...args: any[]): void {
    this.log('info', ...args);
  }

  /**
   * @inheritDoc
   */
  public warn(...args: any[]): void {
    this.log('warn', ...args);
  }

  /**
   * @inheritDoc
   */
  public warning(...args: any[]): void {
    this.warn(...args);
  }

  /**
   * @inheritDoc
   */
  public alert(...args: any[]): void {
    this.log('alert', ...args);
  }

  /**
   * @inheritDoc
   */
  public emerg(...args: any[]): void {
    this.log('emerg', ...args);
  }

  /**
   * @inheritDoc
   */
  public notice(...args: any[]): void {
    this.log('notice', ...args);
  }

  /**
   * @inheritDoc
   */
  public writeLog(options: targetLogger.WriteLogOptions): void {
    // Use the internal log function so we merge the meta data
    const args = options.userArguments.concat();
    if (options.metaData) {
      args.push(options.metaData);
    }
    this.log(options.levelName, ...args);
  }

  /**
   * Initialize the proxy with the given options
   * @param args Possible options
   */
  protected _initialize(...args: any[]) {
    if (_.isObjectLike(args[0])) {
      const options = args[0] as baseProxyLogger.ConstructorOptions;

      if (_.isArray(options.prefixes)) {
        this.properties.prefixes = options.prefixes.filter(item => item.length > 0);
      }

      if (_.isObjectLike(options.parent)) {
        this.properties.parent = options.parent;
      }

      if (_.isObjectLike(options.target)) {
        this.properties.target = options.target;
      }

      if (!_.isNil(options.metaData)) {
        this.properties.metaData = options.metaData;
      }
    }
  }

  /**
   * Configure the arguments to construct the new instance
   * @param constructorArguments
   * @param options
   */
  protected _configureNewInstanceArguments(constructorArguments: Array<baseProxyLogger.ConstructorOptions|any>, options?: genericLogger.OfOptions | string[]) {
    if (_.isNil(options)) {
      return;
    }

    const constructorOptions = constructorArguments[0] as baseProxyLogger.ConstructorOptions;
    const prefixes = [];

    if (_.isArray(options)) {
      prefixes.push(...(options as string[]));
    } else {
      const argsOptions = options as genericLogger.OfOptions;

      if (_.isArray(argsOptions.prefixes)) {
        prefixes.push(...argsOptions.prefixes);
      }

      constructorOptions.metaData = argsOptions.metaData;
    }

    if (prefixes.length > 0) {
      // Force to put the prefixes
      constructorOptions.prefixes = _.uniq(prefixes);
    }
  }

  /**
   * Handle the use log
   * @param options
   */
  protected _handleLog(options: baseProxyLogger.HandleLogOptions): void {
    options.targets.forEach(target => {
      target.writeLog({
        origin: options.origin,
        levelName: options.levelName,
        metaData: options.metaData,
        userArguments: options.userArguments
      });
    });
  }

  /**
   * Select the right target
   */
  protected _selectTargets(): TargetLogger[] {
    if (this.properties.target) {
      return [this.properties.target];
    } else if (!this.properties.parent) {
      return [];
    }

    if (_.isFunction(this.properties.parent['_selectTargets'])) {
      // This is a BaseProxyLogger
      return this.properties.parent['_selectTargets']();
    }

    // Search
    let parent = this.properties.parent;
    let target;

    while (parent && !target) {
      target = parent.target;
      parent = parent.parent;
    }

    return target ? [target] : [];
  }
}

