import Debug = require('debug');
import {SimpleDefaultLoggerHandler} from "./simple-default-logger-handler";
import {LoggerManagerHandler} from "./default-logger-handler";

const debug = Debug('ze:proxy-logger:defaultLoggerManager');
const target = {};

export interface LoggerManagerHandlerService {
  /**
   * Modify the logger manager handler to use
   * @param handler
   */
  setLoggerManagerHandler(handler: LoggerManagerHandler | undefined);

  /**
   * Returns the manager handler
   */
  getLoggerManagerHandler(): LoggerManagerHandler;
}

const proxyHandler = {
  properties: {
    handler: new SimpleDefaultLoggerHandler()
  },
  internalFunctions: {} as LoggerManagerHandlerService,
  get(targetObject: object, key: PropertyKey, receiver: any): any {
    // Make the internal function available
    if (key === '__internalFunctions__') {
      return this.internalFunctions;
    } else if (this.internalFunctions[key]) {
      // Returns the internal function
      return this.internalFunctions[key];
    } else if (!this.properties.handler) {
      return undefined;
    }

    return this.properties.handler[key];
  },
  set(targetObject: object, key: PropertyKey, value: any, receiver: any): boolean {
    if (this.internalFunctions[key] || !this.properties.handler) {
      return false;
    }

    return (this.properties.handler[key] = value);
  },
  deleteProperty(targetObject: object, key: PropertyKey): boolean {
    if (this.internalFunctions[key] || !this.properties.handler) {
      return false;
    }

    return (delete this.properties.handler[key]);
  },
  // enumerate : Obsolete
  ownKeys(targetObject: object): PropertyKey[] {
    if (!this.properties.handler) {
      return [];
    }

    return Reflect.ownKeys(this.properties.handler);
  },
  has(targetObject: object, key: PropertyKey): boolean {
    if (!this.properties.handler) {
      return false;
    }

    return key in this.properties.handler;
  },
  defineProperty(targetObject: object, key: PropertyKey, attributes: PropertyDescriptor): boolean {
    if (!this.properties.handler) {
      return false;
    }

    return Object.defineProperty(this.properties.handler, key, attributes);
  },
  getOwnPropertyDescriptor(targetObject: object, key: PropertyKey): PropertyDescriptor | undefined {
    if (!this.properties.handler) {
      return undefined;
    }

    return Object.getOwnPropertyDescriptor(this.properties.handler, key);
  }
};

proxyHandler.internalFunctions.setLoggerManagerHandler = function (handler) {
  if (handler) {
    debug("Change the handler for a new defined value");
  } else {
    debug("Change the handler for something not defined");
  }
  this.properties.handler = handler || undefined;
}.bind(proxyHandler);

proxyHandler.internalFunctions.getLoggerManagerHandler = function () {
  return this.properties.handler;
}.bind(proxyHandler);

export const defaultLoggerManagerHandler = new Proxy(target, proxyHandler) as LoggerManagerHandler & LoggerManagerHandlerService;

