/**
 * A logger manager
 */
export interface LoggerManager {
  /**
   * Returns the version of the manager (semantic version)
   */
  getVersion(): string;
}

