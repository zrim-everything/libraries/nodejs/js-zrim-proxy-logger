

/**
 * A logger
 */
export interface Logger {
  /**
   * Log data
   *
   * The last argument may be the error object
   */
  log(level: string, ...args: any[]): void;

  /**
   * Log data
   *
   * The last argument may be the error object
   */
  log(level: string, metaData: object | undefined, ...args: any[]): void;

  /**
   * Help end use to know if verbose is enabled in case of optimization
   */
  isVerboseEnabled(): boolean;
}

/**
 * Standard logger
 */
export interface StandardLogger extends Logger {
  /**
   * Log with level: debug
   * @param args
   */
  debug(...args: any[]): void;
  /**
   * Log with level: info
   * @param args
   */
  info(...args: any[]): void;
  /**
   * Log with level: warn
   * @param args
   */
  warn(...args: any[]): void;
  /**
   * Log with level: error
   * @param args
   */
  error(...args: any[]): void;
  /**
   * Log with level: crit
   * @param args
   */
  crit(...args: any[]): void;
}

export interface SyslogLogger extends StandardLogger {
  /**
   * Log with level: silly = debug
   * @param args
   */
  silly(...args: any[]): void;
  /**
   * Log with level: warning = warn
   * @param args
   */
  warning(...args: any[]): void;
  /**
   * Log with level: notice
   * @param args
   */
  notice(...args: any[]): void;
  /**
   * Log with level: alert
   * @param args
   */
  alert(...args: any[]): void;
  /**
   * Log with level: emerg
   * @param args
   */
  emerg(...args: any[]): void;
}
