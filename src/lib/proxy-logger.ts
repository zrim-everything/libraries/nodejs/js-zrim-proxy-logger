import {TargetLogger} from "./target-logger";
import {SyslogLogger} from "./logger";
import {genericLogger, GenericLogger} from "./generic-logger";


/**
 * A proxy logger
 */
export interface ProxyLogger extends GenericLogger, SyslogLogger, TargetLogger {
  /**
   * @inheritDoc
   */
  of(options?: genericLogger.OfOptions): ProxyLogger;
}
