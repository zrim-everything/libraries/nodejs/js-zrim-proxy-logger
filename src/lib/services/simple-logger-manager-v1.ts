import {LoggerManagerV1} from "./logger-manager-v1";
import {ProxyLogger} from "../proxy-logger";
import * as _ from 'lodash';
import Debug = require("debug");
import * as genericProxyLoggerLib from '../simple-generic-proxy-logger';
import {baseProxyLogger} from "../base-proxy-logger";
import {TargetLogger} from "../target-logger";

export namespace simpleLoggerManagerV1 {

  export interface ProxyContainer {
    /**
     * The target logger name
     */
    targetLoggerName: string;

    /**
     * The instance
     */
    instance?: ProxyLogger | undefined;
  }

  export interface ProxyLoggerContainer {
    /**
     * The logger name
     */
    loggerName: string;

    /**
     * The instance
     */
    instance?: TargetLogger | undefined;
  }

  export interface Properties {
    proxies: {[key: string]: ProxyContainer};
    loggers: {[key: string]: ProxyLoggerContainer};
  }

  export interface CreateProxyLoggerOptions {
    /**
     * The logger name
     */
    loggerName: string;
  }

  export interface CreateProxyLoggerOnResolve {
    /**
     * The logger name
     */
    loggerName: string;

    /**
     * The proxy logger
     */
    instance: ProxyLogger;

    /**
     * The target logger name
     */
    targetLoggerName: string;
  }
}

const DEFAULT_NAME = '____default____';
const debug = Debug('ze:proxy-logger:manager:v1');

export class SimpleLoggerManagerV1 implements LoggerManagerV1 {

  /**
   * Internal properties
   */
  protected properties: simpleLoggerManagerV1.Properties;

  constructor() {
    // We make sure this variable is not visible
    Object.defineProperty(this, 'properties', {
      value: {
        proxies: {},
        loggers: {}
      },
      enumerable: false,
      configurable: true
    });
  }

  /**
   * @inheritDoc
   */
  public getVersion(): string {
    return '1.0.0';
  }


  public get defaultLoggerTarget(): TargetLogger | undefined {
    return this.getTargetLogger(DEFAULT_NAME);
  }

  public set defaultLoggerTarget(target: TargetLogger | undefined) {
    // This may give weird thing when we remove the default
    this.setTargetLogger(DEFAULT_NAME, target);
  }

  /**
   * @inheritDoc
   */
  public listLoggerNames(): string[] {
    const loggerNames = _.filter(_.keys(this.properties.proxies), loggerName => {
      return loggerName !== DEFAULT_NAME;
    });

    return loggerNames;
  }

  /**
   * @inheritDoc
   */
  public getTargetLogger(loggerName: string | undefined | null): TargetLogger | undefined {
    if (!loggerName) {
      loggerName = DEFAULT_NAME;
    }

    return this.properties.loggers[loggerName] ? this.properties.loggers[loggerName].instance : undefined;
  }

  /**
   * @inheritDoc
   */
  public setTargetLogger(loggerName: string | undefined | null, target?: TargetLogger | undefined) {
    if (!loggerName) {
      loggerName = DEFAULT_NAME;
    }

    if (_.isNil(target)) {
      debug(`Remove the logger '${loggerName}'`);
      delete this.properties.loggers[loggerName];
    } else {
      debug(`Update the logger '${loggerName}'`);
      this.properties.loggers[loggerName] = {
        instance: target,
        loggerName
      };
    }

    debug(`Update target proxy loggers '${loggerName}'`);
    this._updateTargetProxyLoggers(loggerName, target);
  }

  /**
   * @inheritDoc
   */
  public listTargetLoggerNames(): string[] {
    const loggerNames = _.filter(_.keys(this.properties.loggers), loggerName => {
      return loggerName !== DEFAULT_NAME;
    });

    return loggerNames;
  }

  /**
   * @inheritDoc
   */
  public removeAllTargets(): void {
    // This may be costly in case we set every time the default one : May change the impl. later
    const names = _.keys(this.properties.loggers);

    names.forEach(name => {
      this._updateTargetProxyLoggers(name, undefined);
    });
  }

  /**
   * @inheritDoc
   */
  public getLogger(loggerName?: string | null | undefined): ProxyLogger | undefined {
    if (_.isNil(loggerName) || loggerName.length === 0) {
      loggerName = DEFAULT_NAME;
    }

    let logger = this.properties.proxies[loggerName];
    if (!logger) {
      debug(`Ask to create a new proxy logger '${loggerName}'`);
      logger = this._createProxyLogger({
        loggerName
      });
      debug(`Keep the proxy logger '${loggerName}' in the local database`);
      this.properties.proxies[loggerName] = logger;
    }

    return logger.instance;
  }

  /**
   * Update the target in the proxy loggers
   * @param loggerName The logger name
   * @param target The target to use
   */
  protected _updateTargetProxyLoggers(loggerName: string, target?: TargetLogger | undefined) {
    // Different scenarios:
    // Remove it
    //    We need to set the default target too all using this logger
    // Create or update it
    //    We need to ensure the proxy matching the logger name use this logger.
    if (_.isNil(target)) {
      return this._removeTargetProxyLoggers(loggerName);
    }

    // Search for all proxy logger with the target logger name
    const proxies = this.properties.proxies;
    for (const key in proxies) {
      if (proxies[key].targetLoggerName === loggerName) {
        proxies[key].instance.target = target;
      } else if (key === loggerName) {
        proxies[key].targetLoggerName = loggerName;
        proxies[key].instance.target = target;
      }
    }
  }

  /**
   * Remove the logger in the target. This automatically set the default logger.
   * @param loggerName The logger name
   */
  protected _removeTargetProxyLoggers(loggerName: string) {
    const defaultTarget = this.defaultLoggerTarget;

    const proxies = this.properties.proxies;
    for (const key in proxies) {
      if (proxies[key].targetLoggerName === loggerName) {
        proxies[key].targetLoggerName = DEFAULT_NAME;
        proxies[key].instance.target = defaultTarget;
      }
    }
  }

  /**
   * Create the proxy logger
   * @param options
   */
  protected _createProxyLogger(options: simpleLoggerManagerV1.CreateProxyLoggerOptions): simpleLoggerManagerV1.CreateProxyLoggerOnResolve {
    const {loggerName} = options,
      targetLoggerName = this.properties.loggers[loggerName] ? loggerName : DEFAULT_NAME,
      target = this.properties.loggers[targetLoggerName] ? this.properties.loggers[targetLoggerName].instance : undefined;

    // Create the new proxy
    const proxyLogger = new genericProxyLoggerLib.SimpleGenericProxyLogger({
      target,
      loggerName,
      prefixes: [loggerName]
    } as baseProxyLogger.ConstructorOptions);

    return {
      instance: proxyLogger,
      targetLoggerName,
      loggerName
    };
  }
}
