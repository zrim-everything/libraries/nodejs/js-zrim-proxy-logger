import {LoggerManager} from "./logger-manager";
import {LoggerManagerHandler} from "./default-logger-handler";
import {ProxyLogger} from "./proxy-logger";
import * as _ from 'lodash';
import Debug = require("debug");
import {LoggerManagerV1} from "./services/logger-manager-v1";
import {SimpleLoggerManagerV1} from "./services/simple-logger-manager-v1";
import {TargetLogger} from "./target-logger";


export namespace simpleDefaultLoggerHandler {

  export interface LoggerManagerContainer {
    /**
     * The logger manager
     */
    instance?: LoggerManager;

    /**
     * The logger manager implementation version
     */
    version: number;
  }

  export interface GetLoggerOptions {
    /**
     * The logger manager to use
     */
    loggerManager: LoggerManagerContainer;
    /**
     * The logger name to get
     */
    loggerName: string;
  }

  export interface ListLoggerNamesOptions {
    /**
     * The logger manager to use
     */
    loggerManager: LoggerManagerContainer;
  }

  export interface GetTargetOptions {
    /**
     * The logger manager to use
     */
    loggerManager: LoggerManagerContainer;
    /**
     * The target name to set
     */
    targetName: string;
  }

  export interface SetTargetOptions {
    /**
     * The logger manager to use
     */
    loggerManager: LoggerManagerContainer;
    /**
     * The target name to set
     */
    targetName: string;
    /**
     * The new logger
     */
    target?: TargetLogger | undefined;
  }

  export interface ListTargetNamesOptions {
    /**
     * The logger manager to use
     */
    loggerManager: LoggerManagerContainer;
  }

  export interface Properties {
    /**
     * The logger manager
     */
    loggerManager: LoggerManagerContainer;
  }
}

const defaultLoggerManager = new SimpleLoggerManagerV1();
const debug = Debug('ze:proxy-logger:defaultLoggerManager');

// To make it work
declare const global: any;

/**
 * Simple generic handler for logger
 */
export class SimpleDefaultLoggerHandler implements LoggerManagerHandler {

  /**
   * The properties
   */
  protected properties: simpleDefaultLoggerHandler.Properties;

  /**
   * The default logger manager in cas nothing found
   */
  public get defaultLoggerManager(): LoggerManager {
    return defaultLoggerManager;
  }

  constructor() {
    Object.defineProperty(this, 'properties', {
      value: {
        loggerManager: {
          instance: undefined,
          version: 0
        }
      },
      enumerable: false,
      configurable: true
    });
  }

  /**
   * @inheritDoc
   */
  public getLogger(name?: string): ProxyLogger | undefined {
    // Make sure we always have the logger manager
    this._updateInternalLoggerManager();

    if (!_.isFunction(this[`_getLoggerV${this.properties.loggerManager.version}`])) {
      throw new TypeError(`Cannot handle the version ${this.properties.loggerManager.version}`);
    }

    return this[`_getLoggerV${this.properties.loggerManager.version}`]({
      loggerName: name,
      loggerManager: this.properties.loggerManager
    });
  }

  /**
   * @inheritDoc
   */
  public getTarget(name?: string): TargetLogger | undefined {
    // Make sure we always have the logger manager
    this._updateInternalLoggerManager();

    if (!_.isFunction(this[`_getTargetV${this.properties.loggerManager.version}`])) {
      throw new TypeError(`Cannot handle the version ${this.properties.loggerManager.version}`);
    }

    return this[`_getTargetV${this.properties.loggerManager.version}`]({
      targetName: name,
      loggerManager: this.properties.loggerManager
    });
  }

  /**
   * @inheritDoc
   */
  public setTarget(name?: string, target?: TargetLogger): void {
    // Make sure we always have the logger manager
    this._updateInternalLoggerManager();

    if (!_.isFunction(this[`_setTargetV${this.properties.loggerManager.version}`])) {
      throw new TypeError(`Cannot handle the version ${this.properties.loggerManager.version}`);
    }

    return this[`_setTargetV${this.properties.loggerManager.version}`]({
      targetName: name,
      target,
      loggerManager: this.properties.loggerManager
    });
  }

  /**
   * @inheritDoc
   */
  public listLoggerNames(): string[] {
    // Make sure we always have the logger manager
    this._updateInternalLoggerManager();

    if (!_.isFunction(this[`_listLoggerNamesV${this.properties.loggerManager.version}`])) {
      throw new TypeError(`Cannot handle the version ${this.properties.loggerManager.version}`);
    }

    return this[`_listLoggerNamesV${this.properties.loggerManager.version}`]({
      loggerManager: this.properties.loggerManager
    });
  }

  /**
   * @inheritDoc
   */
  public listTargetNames(): string[] {
    // Make sure we always have the logger manager
    this._updateInternalLoggerManager();

    if (!_.isFunction(this[`_listTargetNamesV${this.properties.loggerManager.version}`])) {
      throw new TypeError(`Cannot handle the version ${this.properties.loggerManager.version}`);
    }

    return this[`_listTargetNamesV${this.properties.loggerManager.version}`]({
      loggerManager: this.properties.loggerManager
    });
  }

  /**
   * Retrieve the logger manager from different places like global variable or local
   */
  protected _retrieveLoggerManager(): LoggerManager | undefined {
    const globalLoggerManager = _.get(global, 'eu.zrimeverything.proxyLogger.defaultLoggerManager') ||
      // Compatible with the old version from js-zrim-core
      _.get(global, 'eu.zrimeverything.core.defaultLoggerManager') ||
      _.get(global, 'jsZrimCore.defaultLoggerManager');

    return globalLoggerManager || this.defaultLoggerManager;
  }

  /**
   * Method in cache or updating the caching logger manager instance
   * @throws {Error} If the manager is invalid
   */
  protected _updateInternalLoggerManager(): void {
    const manager = this._retrieveLoggerManager();
    if (manager === this.properties.loggerManager.instance) {
      return;
    }

    if (!manager) {
      debug("No logger manager found");
      throw new Error(`No logger manager found`); // In theory no manager should not happen due to the default one
    }

    if (!_.isFunction(manager.getVersion)) {
      debug("The logger manager do no contains getVersion");
      throw new TypeError("Invalid logger manager: getVersion is not a function");
    }

    this.properties.loggerManager.instance = manager;
    this.properties.loggerManager.version = this._extractMajorVersionNumber(manager.getVersion());
  }

  /**
   * Extract the major part of the version
   * @param version The sementic version
   * @throws {Error} If the version is invalid
   */
  protected _extractMajorVersionNumber(version: string): number {
    const matches = version.match(/^(\d+)\.(\d+)\.(\d+)$/i);
    if (!_.isArray(matches) || matches.length !== 4) {
      debug("Invalid version format (format error)");
      throw new TypeError(`Invalid version ${version}`);
    }

    return parseInt(matches[1], 10);
  }

  /**
   * Handle the version 1 of the logger manager to get the logger
   * @param options The options
   */
  protected _getLoggerV1(options: simpleDefaultLoggerHandler.GetLoggerOptions): ProxyLogger | undefined {
    // We expected the user to put the right logger
    return (options.loggerManager.instance as LoggerManagerV1).getLogger(options.loggerName);
  }

  /**
   * Handle the version 1 of the logger manager to list logger names
   * @param options The options
   */
  protected _listLoggerNamesV1(options: simpleDefaultLoggerHandler.ListLoggerNamesOptions): string[] {
    // We expected the user to put the right logger
    return (options.loggerManager.instance as LoggerManagerV1).listLoggerNames();
  }

  /**
   * Handle the version 1 of the logger manager to get the target
   * @param options The options
   */
  protected _getTargetV1(options: simpleDefaultLoggerHandler.GetTargetOptions): TargetLogger | undefined {
    // We expected the user to put the right logger
    return (options.loggerManager.instance as LoggerManagerV1).getTargetLogger(options.targetName);
  }

  /**
   * Handle the version 1 of the logger manager to set the logger target
   * @param options The options
   */
  protected _setTargetV1(options: simpleDefaultLoggerHandler.SetTargetOptions): void {
    // We expected the user to put the right logger
    return (options.loggerManager.instance as LoggerManagerV1).setTargetLogger(options.targetName, options.target);
  }

  /**
   * Handle the version 1 of the logger manager to list target names
   * @param options The options
   */
  protected _listTargetNamesV1(options: simpleDefaultLoggerHandler.ListTargetNamesOptions): string[] {
    // We expected the user to put the right logger
    return (options.loggerManager.instance as LoggerManagerV1).listTargetLoggerNames();
  }
}
