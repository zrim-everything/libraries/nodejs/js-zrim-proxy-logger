import {Logger} from "./logger";

export namespace targetLogger {

  export interface WriteLogOptions {
    /**
     * Contains where the call come from
     */
    origin: Logger;

    /**
     * The level name
     */
    levelName: string;

    /**
     * The meta data
     */
    metaData?: object | null | undefined;

    /**
     * The argument given by the user to log
     */
    userArguments: any[];
  }
}


/**
 * A target logger aim to receive a final write
 */
export interface TargetLogger {
  /**
   * Write the log
   * @param options
   */
  writeLog(options: targetLogger.WriteLogOptions): void;

  /**
   * Help end use to know if verbose is enabled in case of optimization
   */
  isVerboseEnabled(): boolean;
}
