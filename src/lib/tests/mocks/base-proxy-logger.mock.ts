import {ProxyLogger} from "../../proxy-logger";
import {SyslogLogger} from "../../logger";
import {genericLogger, GenericLogger} from "../../generic-logger";
import {targetLogger, TargetLogger} from "../../target-logger";

export class BaseProxyLogger implements ProxyLogger, SyslogLogger {
  public metaData: genericLogger.LoggerMetaData;
  public readonly parent: GenericLogger | undefined;
  public prefixes: string[];
  public target: TargetLogger | null | undefined;

  public alert(...args: any[]): void {
  }

  public crit(...args: any[]): void {
  }

  public debug(...args: any[]): void {
  }

  public emerg(...args: any[]): void {
  }

  public error(...args: any[]): void {
  }

  public info(...args: any[]): void {
  }

  public isVerboseEnabled(): boolean {
    return false;
  }

  public log(level: string, ...args: any[]): void;
  public log(level: string, metaData: object | undefined, ...args: any[]): void;
  public log(level: string, ...args: Array<any | object | undefined>): void {
  }

  public notice(...args: any[]): void {
  }

  public of(options?: genericLogger.OfOptions): ProxyLogger;
  public of(options?: genericLogger.OfOptions): GenericLogger;
  public of(options?: genericLogger.OfOptions): ProxyLogger | GenericLogger {
    return new BaseProxyLogger();
  }

  public serializeMetaData(options: genericLogger.SerializeMetaDataOptions): object | undefined {
    return undefined;
  }

  public silly(...args: any[]): void {
  }

  public warn(...args: any[]): void {
  }

  public warning(...args: any[]): void {
  }

  public writeLog(options: targetLogger.WriteLogOptions): void {
  }
}
