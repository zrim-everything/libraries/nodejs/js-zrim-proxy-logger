import {baseProxyLogger, BaseProxyLogger} from '../../../lib/base-proxy-logger';
import {jasmineUtils} from "zrim-test-bootstrap/dist/lib/utils";

describe('#BaseProxyLogger', function () {
  function createInstance(): BaseProxyLogger {
    return new BaseProxyLogger();
  }

  ['alert', 'crit', 'debug', 'emerg', 'error', 'info', 'warn', 'notice'].forEach(name => {
    describe(`#${name}`, function () {
      it("Then must do nothing", function () {
        const instance = createInstance();

        spyOn(instance, 'log');

        instance[name]('pco', '12', 78);
        expect(instance.log).toHaveBeenCalledTimes(1);
        expect(instance.log as any).toHaveBeenCalledWith(name, 'pco', '12', 78);
      });
    });
  });

  [['warning', 'warn'], ['silly', 'debug']].forEach(item => {
    describe(`#${item[0]}`, function () {
      it("Then must do nothing", function () {
        const instance = createInstance();

        spyOn(instance, 'log');

        instance[item[0]]('pco', '12', 78);
        expect(instance.log).toHaveBeenCalledTimes(1);
        expect(instance.log as any).toHaveBeenCalledWith(item[1], 'pco', '12', 78);
      });
    });
  });

  describe('#parent', function () {
    describe('#get', function () {
      it("Given no parent Then must return undefined", function () {
        const instance = createInstance();

        instance['properties'].parent = undefined;
        expect(instance.parent).toBeUndefined();
      });

      it("Given parent Then must return expected value", function () {
        const instance = createInstance();

        instance['properties'].parent = createInstance();
        expect(instance.parent).toBe(instance['properties'].parent);
      });
    }); // #get

    describe('#set', function () {
      it("Given undefined Then must remove parent", function () {
        const instance = createInstance();

        instance['properties'].parent = createInstance();
        instance.parent = undefined;
        expect(instance['properties'].parent).toBeUndefined();
      });

      it("Given parent Then must set parent", function () {
        const instance = createInstance();

        const parent = createInstance();
        instance['properties'].parent = undefined;
        instance.parent = parent;
        expect(instance['properties'].parent).toBe(parent);
      });
    }); // #set
  }); // #parent

  describe('#target', function () {
    describe('#get', function () {
      it("Given no target Then must return expected value", function () {
        const instance = createInstance();

        instance['properties'].target = undefined;
        expect(instance.target).toBeUndefined();
      });

      it("Given target Then must return expected value", function () {
        const instance = createInstance();

        const target = createInstance();
        instance['properties'].target = target;
        expect(instance.target).toBe(target);
      });
    }); // #get

    describe('#set', function () {
      it("Given undefined Then must remove parent", function () {
        const instance = createInstance();

        instance['properties'].target = createInstance();
        instance.target = undefined;
        expect(instance['properties'].target).toBeUndefined();
      });

      it("Given target Then must set target", function () {
        const instance = createInstance();

        const target = createInstance();
        instance['properties'].target = undefined;
        instance.target = target;
        expect(instance['properties'].target).toBe(target);
      });
    }); // #set
  }); // #target

  describe('#target', function () {
    describe('#get', function () {
      it("Then must return expected value", function () {
        const instance = createInstance();

        instance['properties'].prefixes = ['ls', 'ck'];
        expect(instance.prefixes).toEqual(['ls', 'ck']);
      });
    }); // #get

    describe('#set', function () {
      it("Given prefixes with some empty Then must set value", function () {
        const instance = createInstance();

        instance['properties'].prefixes = [];
        instance.prefixes = ['lo', '', 'pljka'];
        expect(instance['properties'].prefixes).toEqual(['lo', 'pljka']);
      });
    }); // #set
  }); // #target

  describe('#metaData', function () {
    describe('#get', function () {
      it("Given no target Then must return expected value", function () {
        const instance = createInstance();

        instance['properties'].metaData = undefined;
        expect(instance.metaData).toBeUndefined();
      });

      it("Given target Then must return expected value", function () {
        const instance = createInstance();

        const metaData = {l: 12};
        instance['properties'].metaData = metaData;
        expect(instance.metaData).toBe(metaData);
      });
    }); // #get

    describe('#set', function () {
      it("Given undefined Then must remove parent", function () {
        const instance = createInstance();

        instance['properties'].metaData = {k: 12};
        instance.metaData = undefined;
        expect(instance['properties'].metaData).toBeUndefined();
      });

      it("Given metaData Then must set metaData", function () {
        const instance = createInstance();

        const metaData = {
          k: 12
        };
        instance['properties'].metaData = undefined;
        instance.metaData = metaData;
        expect(instance['properties'].metaData).toBe(metaData);
      });
    }); // #set
  }); // #metaData

  describe('#isVerboseEnabled', function () {
    it("Given no target Then must return false", function () {
      const instance = createInstance();

      // @ts-ignore
      spyOn(instance, '_selectTargets').and.returnValue([]);
      expect(instance.isVerboseEnabled()).toBe(false);
      expect(instance['_selectTargets']).toHaveBeenCalled();
    });

    it("Given target and return false Then must return false", function () {
      const instance = createInstance();

      const target = jasmineUtils.spyOnClass(BaseProxyLogger);
      target.isVerboseEnabled.and.returnValue(false);

      // @ts-ignore
      spyOn(instance, '_selectTargets').and.returnValue([target]);
      expect(instance.isVerboseEnabled()).toBe(false);
      expect(instance['_selectTargets']).toHaveBeenCalled();
      expect(target.isVerboseEnabled).toHaveBeenCalled();
    });

    it("Given target and return true Then must return true", function () {
      const instance = createInstance();

      const target = jasmineUtils.spyOnClass(BaseProxyLogger);
      target.isVerboseEnabled.and.returnValue(true);

      // @ts-ignore
      spyOn(instance, '_selectTargets').and.returnValue([target]);
      expect(instance.isVerboseEnabled()).toBe(true);
      expect(instance['_selectTargets']).toHaveBeenCalled();
      expect(target.isVerboseEnabled).toHaveBeenCalled();
    });
  }); // #isVerboseEnabled

  describe('#serializeMetaData', function () {
    it("Given metadata as function with error and no parent Them must return expected response", function () {
      const instance = createInstance();

      const error = new Error('Unit Test - Fake Error');
      // @ts-ignore
      const metaData = jasmine.createSpy('metaData').and.throwError(error);
      instance['properties'].metaData = metaData;

      const options = {userArguments: ['kk', 'mk']};
      expect(instance.serializeMetaData(options)).toEqual({
        __Logger_unexpectedError: {
          error,
          stack: error.stack,
          message: error.message
        }
      });
      expect(metaData).toHaveBeenCalledWith(instance, options);
    });

    it("Given meta data function with return undefined and no parent Then must return expected value", function () {
      const instance = createInstance();

      const metaData = jasmine.createSpy('metaData');
      instance['properties'].metaData = metaData;

      const options = {userArguments: ['kk', 'mk']};
      expect(instance.serializeMetaData(options)).toBeUndefined();
      expect(metaData).toHaveBeenCalledWith(instance, options);
    });

    it("Given meta data function with return undefined and prefixes and no parent Then must return expected value", function () {
      const instance = createInstance();

      const metaData = jasmine.createSpy('metaData');
      instance['properties'].metaData = metaData;
      instance['properties'].prefixes = ['ll', 'cvf'];

      const options = {userArguments: ['kk', 'mk']};
      expect(instance.serializeMetaData(options)).toEqual({
        loggerPrefixes: ['ll', 'cvf']
      });
      expect(metaData).toHaveBeenCalledWith(instance, options);
    });

    it("Given meta data function with return data and prefixes and no parent Then must return expected value", function () {
      const instance = createInstance();

      const metaData = jasmine.createSpy('metaData').and.returnValue({
        lk: 45,
        loggerPrefixes: ['ol', 'olcs', 'll']
      });
      instance['properties'].metaData = metaData;
      instance['properties'].prefixes = ['ll', 'cvf'];

      const options = {userArguments: ['kk', 'mk']};
      expect(instance.serializeMetaData(options)).toEqual({
        lk: 45,
        loggerPrefixes: ['ll', 'cvf', 'ol', 'olcs', 'll']
      });
      expect(metaData).toHaveBeenCalledWith(instance, options);
    });

    it("Given meta data object and prefixes and parent Then must return expected value", function () {
      const instance = createInstance();

      const parent = jasmineUtils.spyOnClass(BaseProxyLogger);
      parent.serializeMetaData.and.returnValue({
        kk: 45,
        lk: 968
      });
      instance['properties'].parent = parent;

      instance['properties'].metaData = {
        lk: 45,
        m: 457,
        loggerPrefixes: ['ol', 'olcs', 'll']
      };
      instance['properties'].prefixes = ['ll', 'cvf'];

      const options = {userArguments: ['kk', 'mk']};
      expect(instance.serializeMetaData(options)).toEqual({
        lk: 45,
        loggerPrefixes: ['ll', 'cvf', 'ol', 'olcs', 'll'],
        m: 457,
        kk: 45
      });
    });

    it("Given meta data undefined and prefixes and parent Then must return expected value", function () {
      const instance = createInstance();

      const parent = jasmineUtils.spyOnClass(BaseProxyLogger);
      parent.serializeMetaData.and.returnValue({
        kk: 45,
        lk: 968
      });
      instance['properties'].parent = parent;

      instance['properties'].metaData = undefined;
      instance['properties'].prefixes = ['ll', 'cvf'];

      const options = {userArguments: ['kk', 'mk']};
      expect(instance.serializeMetaData(options)).toEqual({
        lk: 968,
        loggerPrefixes: ['ll', 'cvf'],
        kk: 45
      });
    });

    it("Given meta data string and prefixes and parent Then must return expected value", function () {
      const instance = createInstance();

      const parent = jasmineUtils.spyOnClass(BaseProxyLogger);
      parent.serializeMetaData.and.returnValue({
        kk: 45,
        lk: 968
      });
      instance['properties'].parent = parent;

      instance['properties'].metaData = 'polkjhyhs';
      instance['properties'].prefixes = ['ll', 'cvf'];

      const options = {userArguments: ['kk', 'mk']};
      expect(instance.serializeMetaData(options)).toEqual({
        lk: 968,
        loggerPrefixes: ['ll', 'cvf'],
        kk: 45,
        data: 'polkjhyhs'
      });
    });

    it("Given meta data number and prefixes and parent Then must return expected value", function () {
      const instance = createInstance();

      const parent = jasmineUtils.spyOnClass(BaseProxyLogger);
      parent.serializeMetaData.and.returnValue({
        kk: 45,
        lk: 968
      });
      instance['properties'].parent = parent;

      instance['properties'].metaData = 884455;
      instance['properties'].prefixes = ['ll', 'cvf'];

      const options = {userArguments: ['kk', 'mk']};
      expect(instance.serializeMetaData(options)).toEqual({
        lk: 968,
        loggerPrefixes: ['ll', 'cvf'],
        kk: 45,
        data: 884455
      });
    });

    it("Given meta data false and prefixes and parent Then must return expected value", function () {
      const instance = createInstance();

      const parent = jasmineUtils.spyOnClass(BaseProxyLogger);
      parent.serializeMetaData.and.returnValue({
        kk: 45,
        lk: 968
      });
      instance['properties'].parent = parent;

      instance['properties'].metaData = false;
      instance['properties'].prefixes = ['ll', 'cvf'];

      const options = {userArguments: ['kk', 'mk']};
      expect(instance.serializeMetaData(options)).toEqual({
        lk: 968,
        loggerPrefixes: ['ll', 'cvf'],
        kk: 45,
        data: false
      });
    });

    it("Given meta data true and prefixes and parent Then must return expected value", function () {
      const instance = createInstance();

      const parent = jasmineUtils.spyOnClass(BaseProxyLogger);
      parent.serializeMetaData.and.returnValue({
        kk: 45,
        lk: 968
      });
      instance['properties'].parent = parent;

      instance['properties'].metaData = true;
      instance['properties'].prefixes = ['ll', 'cvf'];

      const options = {userArguments: ['kk', 'mk']};
      expect(instance.serializeMetaData(options)).toEqual({
        lk: 968,
        loggerPrefixes: ['ll', 'cvf'],
        kk: 45,
        data: true
      });
    });
  }); // #serializeMetaData

  describe('#writeLog', function () {
    it("Given no metadata Then must call it with args", function () {
      const instance = createInstance();

      spyOn(instance, 'log');
      const options = {
        levelName: 'olk',
        userArguments: ['jjc', 12],
        origin: instance
      };
      instance.writeLog(options);
      expect(instance.log).toHaveBeenCalledTimes(1);
      expect(instance.log as any).toHaveBeenCalledWith('olk', 'jjc', 12);
    });

    it("Given metadata Then must call it with args and metadata", function () {
      const instance = createInstance();

      spyOn(instance, 'log');
      const options = {
        levelName: 'olk',
        userArguments: ['jjc', 12],
        origin: instance,
        metaData: {
          jh: 78
        }
      };
      instance.writeLog(options);
      expect(instance.log).toHaveBeenCalledTimes(1);
      expect(instance.log as any).toHaveBeenCalledWith('olk', 'jjc', 12, {
        jh: 78
      });
    });
  }); // #writeLog

  describe('#of', function () {
    it("Then must call _configureNewInstanceArguments and return expected value", function () {

      const constructorCall = jasmine.createSpy('constructor');

      class TestA extends BaseProxyLogger {
        constructor(options?) {
          super();
          constructorCall(options);
        }
      }

      const instance = new TestA();
      const target = createInstance();

      // @ts-ignore
      spyOn(instance, '_configureNewInstanceArguments').and.callFake(options => {
        expect(options).toEqual([{
          target,
          parent: instance
        }]);
        options[0].pl = 45;
      });

      const options = {
        p: 41,
        o: 45,
        target
      };
      const result = instance.of(options);
      expect(result).toEqual(jasmine.any(TestA));
      expect(instance['_configureNewInstanceArguments']).toHaveBeenCalledTimes(1);
      expect(instance['_configureNewInstanceArguments']).toHaveBeenCalledWith(jasmine.any(Object), options);
      expect(constructorCall).toHaveBeenCalledWith({
        target,
        parent: instance,
        pl: 45
      });
    });
  }); // #of

  describe('#_initialize', function () {
    function createInstance() {
      const instance = Object.create(BaseProxyLogger.prototype);

      instance.properties = {
        parent: undefined,
        target: undefined,
        prefixes: [],
        metaData: undefined
      };

      return instance;
    }

    it("Given options not object Then must do nothing", function () {
      const instance = createInstance();

      instance['_initialize']('');
      expect(instance['properties']).toEqual({
        parent: undefined,
        target: undefined,
        prefixes: [],
        metaData: undefined
      });
    });

    it("Given options without known attributes Then must do nothing", function () {
      const instance = createInstance();

      instance['_initialize']({c: 12});
      expect(instance['properties']).toEqual({
        parent: undefined,
        target: undefined,
        prefixes: [],
        metaData: undefined
      });
    });

    it("Given options with known attributes Then must set them", function () {
      const instance = createInstance();

      const options = {
        parent: createInstance(),
        target: createInstance(),
        prefixes: ['lld', '', 'csa'],
        metaData: {
          l: 451
        },
        c: 12
      };
      options.parent['sd'] = 554;
      options.target['sdf'] = 554;

      instance['_initialize'](options);
      expect(instance['properties']).toEqual({
        parent: options.parent,
        target: options.target,
        prefixes: ['lld', 'csa'],
        metaData: {
          l: 451
        }
      });
    });
  }); // #_initialize

  describe('#_configureNewInstanceArguments', function () {
    it("Given nil options Then must do nothing", function () {
      const instance = createInstance();

      const parent = createInstance();
      const constructorArgument = {
        parent
      };
      const options = undefined;

      instance['_configureNewInstanceArguments']([constructorArgument], options);
      expect(constructorArgument).toEqual({
        parent
      });
    });

    it("Given options as string[] Then must do nothing", function () {
      const instance = createInstance();

      const parent = createInstance();
      const constructorArgument = {
        parent
      } as baseProxyLogger.ConstructorOptions;
      const options = ['cxl', 'osais', 'cxl'];

      instance['_configureNewInstanceArguments']([constructorArgument], options);
      expect(constructorArgument).toEqual({
        parent,
        prefixes: ['cxl', 'osais']
      });
    });

    it("Given options empty object Then must do nothing", function () {
      const instance = createInstance();

      const parent = createInstance();
      const constructorArgument = {
        parent
      } as baseProxyLogger.ConstructorOptions;
      const options = {};

      instance['_configureNewInstanceArguments']([constructorArgument], options);
      expect(constructorArgument).toEqual({
        parent,
        metaData: undefined
      });
    });

    it("Given options Then must do nothing", function () {
      const instance = createInstance();

      const parent = createInstance();
      const constructorArgument = {
        parent
      } as baseProxyLogger.ConstructorOptions;
      const options = {
        prefixes: ['cxl', 'osais', 'cxl'],
        metaData: {
          lk: 12
        }
      };

      instance['_configureNewInstanceArguments']([constructorArgument], options);
      expect(constructorArgument).toEqual({
        parent,
        prefixes: ['cxl', 'osais'],
        metaData: {
          lk: 12
        }
      });
    });
  }); // #_configureNewInstanceArguments

  describe('#_handleLog', function () {
    it("Given targets Then must call writeLog", function () {
      const instance = createInstance();

      const options = {
        targets: [jasmineUtils.spyOnClass(BaseProxyLogger), jasmineUtils.spyOnClass(BaseProxyLogger)],
        origin: instance,
        levelName: 'lclck',
        metaData: {
          lk: 45
        },
        userArguments: ['skks', 'cxjhx'],
        rawUserArguments: ['skks', 'cxjhx', 'po']
      };

      instance['_handleLog'](options);
      options.targets.forEach(target => {
        expect(target.writeLog).toHaveBeenCalledTimes(1);
        expect(target.writeLog).toHaveBeenCalledWith({
          origin: instance,
          levelName: 'lclck',
          metaData: {
            lk: 45
          },
          userArguments: ['skks', 'cxjhx']
        });
      });
    });
  }); // #_handleLog

  describe('#_selectTargets', function () {
    it("Given target Then must return expected value", function () {
      const instance = createInstance();

      instance['properties'].target = jasmineUtils.spyOnClass(BaseProxyLogger);
      expect(instance['_selectTargets']()).toEqual([instance['properties'].target]);
    });

    it("Given no target and no parent Then must return expected value", function () {
      const instance = createInstance();

      instance['properties'].target = undefined;
      instance['properties'].parent = undefined;
      expect(instance['_selectTargets']()).toEqual([]);
    });

    it("Given parent with _selectTargets Then must call it and return expected response", function () {
      const instance = createInstance();

      const parent = jasmineUtils.spyOnClass(BaseProxyLogger);
      const targets = [createInstance(), createInstance()];
      // @ts-ignore
      parent['_selectTargets'].and.returnValue(targets);

      instance['properties'].parent = parent;
      expect(instance['_selectTargets']()).toEqual(targets);
    });

    it("Given parent and children without target Then must return expected value", function () {
      const instance = createInstance();

      const parent = {
        parent: {
          parent: {

          }
        }
      };

      // @ts-ignore
      instance['properties'].parent = parent;
      expect(instance['_selectTargets']()).toEqual([]);
    });

    it("Given parent and children Then must return expected value", function () {
      const instance = createInstance();


      const targets = [createInstance()];

      const parent = {
        parent: {
          target: targets[0]
        }
      };

      // @ts-ignore
      instance['properties'].parent = parent;
      expect(instance['_selectTargets']()).toEqual(targets);
    });
  }); // #_selectTargets

  describe('#log', function () {
    it("Given invalid log level Then must do nothing", function () {
      const instance = createInstance();

      // @ts-ignore
      spyOn(instance, '_selectTargets');

      // @ts-ignore
      instance.log();
      // @ts-ignore
      instance.log({});
      // @ts-ignore
      instance.log(12);

      expect(instance['_selectTargets']).not.toHaveBeenCalled();
    });

    it("Given no targets Then must do nothing", function () {
      const instance = createInstance();

      // @ts-ignore
      spyOn(instance, '_selectTargets').and.returnValue([]);

      spyOn(instance, 'serializeMetaData');

      // @ts-ignore
      spyOn(instance, '_handleLog');

      instance.log('lsksk');

      expect(instance['_selectTargets']).toHaveBeenCalled();
      expect(instance['serializeMetaData']).not.toHaveBeenCalled();
      expect(instance['_handleLog']).not.toHaveBeenCalled();
    });

    it("Given targets Then must call handleLog", function () {
      const instance = createInstance();

      const target = createInstance();

      // @ts-ignore
      spyOn(instance, '_selectTargets').and.returnValue([target]);

      spyOn(instance, 'serializeMetaData');

      // @ts-ignore
      spyOn(instance, '_handleLog');

      instance.log('lsksk');

      expect(instance['_selectTargets']).toHaveBeenCalled();
      expect(instance['serializeMetaData']).toHaveBeenCalledWith({
        userArguments: []
      });
      expect(instance['_handleLog']).toHaveBeenCalledWith({
        origin: instance,
        targets: [target],
        levelName: 'lsksk',
        metaData: {},
        userArguments: [],
        rawUserArguments: []
      });
    });

    it("Given metaData as first argument and error in last Then must call handleLog", function () {
      const instance = createInstance();

      const target = createInstance();

      // @ts-ignore
      spyOn(instance, '_selectTargets').and.returnValue([target]);

      spyOn(instance, 'serializeMetaData');

      // @ts-ignore
      spyOn(instance, '_handleLog');

      const rawArgs = [{K: 45}, 'pooc', new Error('Unit Test - Fake Error')];
      instance.log('lsksk', ...rawArgs.concat());

      expect(instance['_selectTargets']).toHaveBeenCalled();
      expect(instance['serializeMetaData']).toHaveBeenCalledWith({
        userArguments: ['pooc']
      });
      expect(instance['_handleLog']).toHaveBeenCalledWith({
        origin: instance,
        targets: [target],
        levelName: 'lsksk',
        metaData: {
          K: 45,
          error: rawArgs[2]
        },
        userArguments: ['pooc'],
        rawUserArguments: rawArgs
      });
    });

    it("Given metaData as last argument and error in first Then must call handleLog", function () {
      const instance = createInstance();

      const target = createInstance();

      // @ts-ignore
      spyOn(instance, '_selectTargets').and.returnValue([target]);

      spyOn(instance, 'serializeMetaData');

      // @ts-ignore
      spyOn(instance, '_handleLog');

      const rawArgs = [new Error('Unit Test - Fake Error'), 'pooc', {K: 45}];
      instance.log('lsksk', ...rawArgs.concat());

      expect(instance['_selectTargets']).toHaveBeenCalled();
      expect(instance['serializeMetaData']).toHaveBeenCalledWith({
        userArguments: ['pooc']
      });
      expect(instance['_handleLog']).toHaveBeenCalledWith({
        origin: instance,
        targets: [target],
        levelName: 'lsksk',
        metaData: {
          K: 45,
          error: rawArgs[0]
        },
        userArguments: ['pooc'],
        rawUserArguments: rawArgs
      });
    });

    it("Given metaData as fist and last and internal one Then must call handleLog", function () {
      const instance = createInstance();

      const target = createInstance();

      // @ts-ignore
      spyOn(instance, '_selectTargets').and.returnValue([target]);

      spyOn(instance, 'serializeMetaData').and.returnValue({
        n: 10, k: 7458
      });

      // @ts-ignore
      spyOn(instance, '_handleLog');

      const rawArgs = [{l: 41, k: 126}, 'pooc', 'po', {K: 45, l: 58}];
      instance.log('lsksk', ...rawArgs.concat());

      expect(instance['_selectTargets']).toHaveBeenCalled();
      expect(instance['serializeMetaData']).toHaveBeenCalledWith({
        userArguments: ['pooc', 'po']
      });
      expect(instance['_handleLog']).toHaveBeenCalledWith({
        origin: instance,
        targets: [target],
        levelName: 'lsksk',
        metaData: {
          K: 45,
          k: 126,
          n: 10,
          l: 58
        },
        userArguments: ['pooc', 'po'],
        rawUserArguments: rawArgs
      });
    });
  }); // #log
}); // #BaseProxyLogger

