import {defaultLoggerManagerHandler} from '../../../lib/default-logger';
import {SimpleDefaultLoggerHandler} from "../../../lib/simple-default-logger-handler";
import {jasmineUtils} from "zrim-test-bootstrap/dist/lib/utils";

describe('#default-logger', function () {

  describe('#getLoggerManagerHandler', function () {
    it("Then must return default handler", function () {
      expect(defaultLoggerManagerHandler.getLoggerManagerHandler()).toEqual(jasmine.any(SimpleDefaultLoggerHandler));
    });
  }); // #getLoggerManagerHandler

  describe('#setLoggerManagerHandler', function () {
    it("Given undefined Then must remove handler", function () {
      expect(defaultLoggerManagerHandler.getLoggerManagerHandler()).not.toBeUndefined();
      defaultLoggerManagerHandler.setLoggerManagerHandler(undefined);
      expect(defaultLoggerManagerHandler.getLoggerManagerHandler()).toBeUndefined();
    });

    it("Given new handler Then must set it", function () {
      const handler = jasmineUtils.spyOnClass(SimpleDefaultLoggerHandler);
      defaultLoggerManagerHandler.setLoggerManagerHandler(handler);
      expect(defaultLoggerManagerHandler.getLoggerManagerHandler()).toEqual(handler);
    });
  }); // #setLoggerManagerHandler

  describe('#get', function () {
    it('Given __internalFunctions__ Then must return the internal object', function () {
      expect(defaultLoggerManagerHandler['__internalFunctions__']).toEqual({
        getLoggerManagerHandler: jasmine.any(Function),
        setLoggerManagerHandler: jasmine.any(Function)
      });
    });

    it("Given key known in internal Then must return expected value", function () {
      const internal = defaultLoggerManagerHandler['__internalFunctions__'];
      expect(defaultLoggerManagerHandler['getLoggerManagerHandler']).toBe(internal['getLoggerManagerHandler']);
    });

    it("Given key not internal and handler not defined Then must return undefined", function () {
      defaultLoggerManagerHandler.setLoggerManagerHandler(undefined);
      expect(defaultLoggerManagerHandler['ujdd']).toBeUndefined();
    });

    it("Given key not internal and handler Then must return value from handler", function () {
      const handler = jasmineUtils.spyOnClass(SimpleDefaultLoggerHandler);
      handler['jududu'] = '454d';
      defaultLoggerManagerHandler.setLoggerManagerHandler(handler);
      expect(defaultLoggerManagerHandler['jududu']).toBe(handler['jududu']);
    });
  }); // #get

  describe('#set', function () {
    it("Given no handler and key internal Then must return error", function () {
      expect(() => {
        // @ts-ignore
        defaultLoggerManagerHandler['setLoggerManagerHandler'] = 'po';
      }).toThrow(jasmine.any(TypeError));
    });

    it("Given no handler and key not internal Then must return error", function () {
      defaultLoggerManagerHandler.setLoggerManagerHandler(undefined);
      expect(() => {
        // @ts-ignore
        defaultLoggerManagerHandler['lks'] = 'po';
      }).toThrow(jasmine.any(TypeError));
    });

    it("Given handler and key not internal Then must set value", function () {
      const handler = jasmineUtils.spyOnClass(SimpleDefaultLoggerHandler);
      handler['jududu'] = '454d';
      defaultLoggerManagerHandler.setLoggerManagerHandler(handler);
      defaultLoggerManagerHandler['jududu'] = 'po';
      expect(handler['jududu']).toBe('po');
    });
  }); // #set

  describe('#delete', function () {
    it("Given no handler and key internal Then must return error", function () {
      expect(() => {
        delete defaultLoggerManagerHandler['setLoggerManagerHandler'];
      }).toThrow(jasmine.any(TypeError));
    });

    it("Given no handler and key not internal Then must return error", function () {
      defaultLoggerManagerHandler.setLoggerManagerHandler(undefined);
      expect(() => {
        delete defaultLoggerManagerHandler['lks'];
      }).toThrow(jasmine.any(TypeError));
    });

    it("Given handler and key not internal Then must set value", function () {
      const handler = jasmineUtils.spyOnClass(SimpleDefaultLoggerHandler);
      handler['jududu'] = '454d';
      defaultLoggerManagerHandler.setLoggerManagerHandler(handler);
      delete defaultLoggerManagerHandler['jududu'];
      expect(handler['jududu']).toBeUndefined();
    });
  }); // #delete

  describe('#ownKeys', function () {
    it("Given no handler Then must no for-loop", function () {
      defaultLoggerManagerHandler.setLoggerManagerHandler(undefined);

      /* tslint:disable:forin */
      for (const key in defaultLoggerManagerHandler) {
        fail(new Error('Must not enumerate'));
      }
      /* tslint:enable:forin */

      expect(true).toBeTruthy();
    });

    it("Given handler Then must for-loop", function () {
      const handler = jasmineUtils.spyOnClass(SimpleDefaultLoggerHandler);
      defaultLoggerManagerHandler.setLoggerManagerHandler(handler);

      const keys = [];
      /* tslint:disable:forin */
      for (const key in defaultLoggerManagerHandler) {
        keys.push(key);
      }
      /* tslint:enable:forin */

      expect(keys.length).toBeGreaterThan(0);
    });
  }); // #ownKeys

  describe('#has', function () {
    it("Given no handler and key internal Then must return false", function () {
      defaultLoggerManagerHandler.setLoggerManagerHandler(undefined);

      expect('setLoggerManagerHandler' in defaultLoggerManagerHandler).toBe(false);
    });

    it("Given no handler and key not internal Then must return false", function () {
      defaultLoggerManagerHandler.setLoggerManagerHandler(undefined);

      expect('asasa' in defaultLoggerManagerHandler).toBe(false);
    });

    it("Given handler and key not present Then must return false", function () {
      const handler = jasmineUtils.spyOnClass(SimpleDefaultLoggerHandler);
      defaultLoggerManagerHandler.setLoggerManagerHandler(handler);

      expect('asasa' in defaultLoggerManagerHandler).toBe(false);
    });

    it("Given handler and key present Then must return true", function () {
      const handler = jasmineUtils.spyOnClass(SimpleDefaultLoggerHandler);
      defaultLoggerManagerHandler.setLoggerManagerHandler(handler);

      handler['asasa'] = undefined;
      expect('asasa' in defaultLoggerManagerHandler).toBe(true);

      handler['asasa'] = 'pool';
      expect('asasa' in defaultLoggerManagerHandler).toBe(true);
    });
  }); // #has

  describe('#defineProperty', function () {
    it("Given no handler Then must throw error", function () {
      defaultLoggerManagerHandler.setLoggerManagerHandler(undefined);

      expect(() => {
        Object.defineProperty(defaultLoggerManagerHandler, 'albac', {
          value: 12
        });
      }).toThrow(jasmine.any(TypeError));
    });

    it("Given handler Then must set property", function () {
      const handler = jasmineUtils.spyOnClass(SimpleDefaultLoggerHandler);
      defaultLoggerManagerHandler.setLoggerManagerHandler(handler);

      Object.defineProperty(defaultLoggerManagerHandler, 'albac', {
        value: 12,
        enumerable: false,
        configurable: true,
        writable: false
      });

      expect(Object.getOwnPropertyDescriptor(defaultLoggerManagerHandler, 'albac')).toEqual({
        value: 12,
        enumerable: false,
        configurable: true,
        writable: false
      });
    });
  }); // #defineProperty

  describe('#getOwnPropertyDescriptor', function () {
    it("Given no handler Then must return undefined", function () {
      defaultLoggerManagerHandler.setLoggerManagerHandler(undefined);
      expect(Object.getOwnPropertyDescriptor(defaultLoggerManagerHandler, 'albac')).toBeUndefined();
    });

    it("Given handler and not defined Then must return undefined", function () {
      const handler = jasmineUtils.spyOnClass(SimpleDefaultLoggerHandler);
      defaultLoggerManagerHandler.setLoggerManagerHandler(handler);
      expect(Object.getOwnPropertyDescriptor(defaultLoggerManagerHandler, 'albac')).toBeUndefined();
    });

    it("Given handler and defined Then must return expected value", function () {
      const handler = jasmineUtils.spyOnClass(SimpleDefaultLoggerHandler);
      defaultLoggerManagerHandler.setLoggerManagerHandler(handler);

      Object.defineProperty(handler, 'albac', {
        value: 12,
        enumerable: false,
        configurable: true,
        writable: false
      });

      expect(Object.getOwnPropertyDescriptor(defaultLoggerManagerHandler, 'albac')).toEqual({
        value: 12,
        enumerable: false,
        configurable: true,
        writable: false
      });
    });
  }); // #getOwnPropertyDescriptor
});

