import {SimpleLoggerManagerV1} from '../../../../lib/services/simple-logger-manager-v1';
import {jasmineUtils} from "zrim-test-bootstrap/dist/lib/utils";
import {BaseProxyLogger} from "../../../../lib";
import * as genericLoggerLib from '../../../../lib/simple-generic-proxy-logger';
import {TargetLogger, targetLogger} from "../../../../lib/target-logger";


describe('#SimpleLoggerManagerV1', function () {
  function createInstance(): SimpleLoggerManagerV1 {
    return new SimpleLoggerManagerV1();
  }

  describe('#construct', function () {
    it("Then must create the properties property", function () {
      const instance = new SimpleLoggerManagerV1();

      expect(instance['properties']).toEqual({
        proxies: {},
        loggers: {}
      });
    });
  }); // #construct

  describe('#defaultLoggerTarget', function () {
    describe('#get', function () {
      it("Then must call getTargetLogger and return expected value", function () {
        const instance = createInstance();

        const expectedValue = jasmineUtils.spyOnClass(BaseProxyLogger) as BaseProxyLogger;

        spyOn(instance, 'getTargetLogger').and.returnValue(expectedValue);
        expect(instance.defaultLoggerTarget).toEqual(expectedValue);
        expect(instance.getTargetLogger).toHaveBeenCalledWith('____default____');
      });
    }); // #get

    describe('#set', function () {
      it("Then must call getTargetLogger and return expected value", function () {
        const instance = createInstance();

        const expectedValue = jasmineUtils.spyOnClass(BaseProxyLogger) as BaseProxyLogger;
        spyOn(instance, 'setTargetLogger');
        instance.defaultLoggerTarget = expectedValue;
        expect(instance.setTargetLogger).toHaveBeenCalledWith('____default____', expectedValue);
      });
    }); // #set
  }); // #defaultLoggerTarget

  describe('#getVersion', function () {
    it("Then must return the right version", function () {
      const instance = createInstance();

      expect(instance.getVersion()).toBe('1.0.0');
    });
  }); // #getVersion

  describe('#listLoggerNames', function () {
    it("Given default name and other Then must return expected value", function () {
      const instance = createInstance();

      instance['properties'].proxies = {
        ____default____: {
          targetLoggerName: '____default____',
          instance: jasmineUtils.spyOnClass(BaseProxyLogger) as BaseProxyLogger
        },
        ab: {
          targetLoggerName: 'ab',
          instance: jasmineUtils.spyOnClass(BaseProxyLogger) as BaseProxyLogger
        },
        hn: {
          targetLoggerName: 'hn',
          instance: jasmineUtils.spyOnClass(BaseProxyLogger) as BaseProxyLogger
        },
        up: {
          targetLoggerName: 'up',
          instance: jasmineUtils.spyOnClass(BaseProxyLogger) as BaseProxyLogger
        }
      };
      expect(instance.listLoggerNames()).toEqual(jasmine.arrayContaining(['ab', 'hn', 'up']));
    });
  }); // #listLoggerNames

  describe('#listTargetLoggerNames', function () {
    it("Given default name and other Then must return expected value", function () {
      const instance = createInstance();

      instance['properties'].loggers = {
        ____default____: {
          loggerName: '____default____',
          instance: jasmineUtils.spyOnClass(BaseProxyLogger) as BaseProxyLogger
        },
        ab: {
          loggerName: 'ab',
          instance: jasmineUtils.spyOnClass(BaseProxyLogger) as BaseProxyLogger
        },
        hn: {
          loggerName: 'hn',
          instance: jasmineUtils.spyOnClass(BaseProxyLogger) as BaseProxyLogger
        },
        up: {
          loggerName: 'up',
          instance: jasmineUtils.spyOnClass(BaseProxyLogger) as BaseProxyLogger
        }
      };
      expect(instance.listTargetLoggerNames()).toEqual(jasmine.arrayContaining(['ab', 'hn', 'up']));
    });
  }); // #listTargetLoggerNames

  describe('#getLogger', function () {
    it("Given logger name nil and logger not present Then must create new proxy and return it", function () {
      const instance = createInstance();

      const logger = {
        instance: jasmineUtils.spyOnClass(BaseProxyLogger) as BaseProxyLogger,
        loggerName: 'kl',
        targetLoggerName: 'yhh'
      };
      // @ts-ignore
      spyOn(instance, '_createProxyLogger').and.returnValue(logger);

      const result = instance.getLogger();
      expect(result).toEqual(logger.instance);
      expect(instance['_createProxyLogger']).toHaveBeenCalledTimes(1);
      expect(instance['_createProxyLogger']).toHaveBeenCalledWith({
        loggerName: '____default____'
      });

      expect(instance['properties'].proxies).toEqual({
        ____default____: logger
      });
    });

    it("Given logger name empty and logger present Then must return it", function () {
      const instance = createInstance();

      const logger = {
        instance: jasmineUtils.spyOnClass(BaseProxyLogger) as BaseProxyLogger,
        loggerName: 'kl',
        targetLoggerName: 'yhh'
      };
      // @ts-ignore
      spyOn(instance, '_createProxyLogger');

      instance['properties'].proxies = {
        ____default____: logger
      };

      const result = instance.getLogger('');
      expect(result).toEqual(logger.instance);
      expect(instance['_createProxyLogger']).not.toHaveBeenCalled();
    });

    it("Given logger name and logger present Then must return it", function () {
      const instance = createInstance();

      const logger = {
        instance: jasmineUtils.spyOnClass(BaseProxyLogger) as BaseProxyLogger,
        loggerName: 'kl',
        targetLoggerName: 'yhh'
      };
      // @ts-ignore
      spyOn(instance, '_createProxyLogger');

      instance['properties'].proxies = {
        acbv: logger
      };

      const result = instance.getLogger('acbv');
      expect(result).toEqual(logger.instance);
      expect(instance['_createProxyLogger']).not.toHaveBeenCalled();
    });
  }); // #getLogger

  describe('#_createProxyLogger', function () {
    it("Given target logger not present and no target Then must create and return default one", function () {
      const instance = createInstance();

      const proxyInstance = jasmineUtils.spyOnClass(genericLoggerLib.SimpleGenericProxyLogger);
      // @ts-ignore
      spyOn(genericLoggerLib, 'SimpleGenericProxyLogger').and.returnValue(proxyInstance);

      const options = {
        loggerName: 'ol'
      };
      expect(instance['_createProxyLogger'](options)).toEqual({
        instance: proxyInstance,
        loggerName: 'ol',
        targetLoggerName: '____default____'
      });
      expect(genericLoggerLib.SimpleGenericProxyLogger).toHaveBeenCalledWith({
        prefixes: ['ol'],
        target: undefined,
        loggerName: 'ol'
      });
    });

    it("Given target logger present and target Then must create and return expected value", function () {
      const instance = createInstance();

      const proxyInstance = jasmineUtils.spyOnClass(genericLoggerLib.SimpleGenericProxyLogger);
      // @ts-ignore
      spyOn(genericLoggerLib, 'SimpleGenericProxyLogger').and.returnValue(proxyInstance);

      instance['properties'].loggers = {
        ol: {
          instance: jasmineUtils.spyOnClass(BaseProxyLogger),
          loggerName: 'ol'
        }
      };

      const options = {
        loggerName: 'ol'
      };
      expect(instance['_createProxyLogger'](options)).toEqual({
        instance: proxyInstance,
        loggerName: 'ol',
        targetLoggerName: 'ol'
      });
      expect(genericLoggerLib.SimpleGenericProxyLogger).toHaveBeenCalledWith({
        prefixes: ['ol'],
        target: instance['properties'].loggers.ol.instance,
        loggerName: 'ol'
      });
    });
  }); // #_createProxyLogger

  describe('#getTargetLogger', function () {
    it("Given logger not present Then must return undefined", function () {
      const instance = createInstance();

      expect(instance.getTargetLogger('ol')).toBeUndefined();
    });

    it("Given no logger name and logger present Then must return expected value", function () {
      const instance = createInstance();

      const target = {
        instance: jasmineUtils.spyOnClass(BaseProxyLogger),
        loggerName: '____default____'
      };
      instance['properties'].loggers['____default____'] = target;

      expect(instance.getTargetLogger(null)).toEqual(target.instance);
    });

    it("Given logger present Then must return expected value", function () {
      const instance = createInstance();

      const target = {
        instance: jasmineUtils.spyOnClass(BaseProxyLogger),
        loggerName: 'ol'
      };
      instance['properties'].loggers['ol'] = target;

      expect(instance.getTargetLogger('ol')).toEqual(target.instance);
    });
  }); // #getTargetLogger

  describe('#setTargetLogger', function () {
    it("Given target nil and not present Then must call _updateTargetProxyLoggers", function () {
      const instance = createInstance();

      // @ts-ignore
      spyOn(instance, '_updateTargetProxyLoggers');

      instance.setTargetLogger('ovc', null);

      expect(instance['_updateTargetProxyLoggers']).toHaveBeenCalledTimes(1);
      expect(instance['_updateTargetProxyLoggers']).toHaveBeenCalledWith('ovc', null);
      expect(instance['properties'].loggers.ovc).toBeUndefined();
    });

    it("Given target nil and present Then must call _updateTargetProxyLoggers and delete logger", function () {
      const instance = createInstance();

      const target = {
        instance: jasmineUtils.spyOnClass(BaseProxyLogger),
        loggerName: 'ol'
      };
      instance['properties'].loggers['ovc'] = target;

      // @ts-ignore
      spyOn(instance, '_updateTargetProxyLoggers');

      instance.setTargetLogger('ovc', undefined);
      expect(instance['_updateTargetProxyLoggers']).toHaveBeenCalledTimes(1);
      expect(instance['_updateTargetProxyLoggers']).toHaveBeenCalledWith('ovc', undefined);
      expect(instance['properties'].loggers.ovc).toBeUndefined();
    });

    it("Given target and present Then must call _updateTargetProxyLoggers and update logger", function () {
      const instance = createInstance();

      instance['properties'].loggers['ovc'] = {
        instance: jasmineUtils.spyOnClass(BaseProxyLogger),
        loggerName: 'ol'
      };

      // @ts-ignore
      spyOn(instance, '_updateTargetProxyLoggers');
      const target = jasmineUtils.spyOnClass(BaseProxyLogger);

      instance.setTargetLogger('ovc', target);

      expect(instance['_updateTargetProxyLoggers']).toHaveBeenCalledTimes(1);
      expect(instance['_updateTargetProxyLoggers']).toHaveBeenCalledWith('ovc', target);
      expect(instance['properties'].loggers.ovc).toEqual({
        instance: target,
        loggerName: 'ovc'
      });
    });

    it("Given no logger name target and present Then must call _updateTargetProxyLoggers and update logger", function () {
      const instance = createInstance();

      instance['properties'].loggers['____default____'] = {
        instance: jasmineUtils.spyOnClass(BaseProxyLogger),
        loggerName: '____default____'
      };

      // @ts-ignore
      spyOn(instance, '_updateTargetProxyLoggers');
      const target = jasmineUtils.spyOnClass(BaseProxyLogger);

      instance.setTargetLogger(null, target);

      expect(instance['_updateTargetProxyLoggers']).toHaveBeenCalledTimes(1);
      expect(instance['_updateTargetProxyLoggers']).toHaveBeenCalledWith('____default____', target);
      expect(instance['properties'].loggers['____default____']).toEqual({
        instance: target,
        loggerName: '____default____'
      });
    });
  }); // #setTargetLogger

  describe('#removeAllTargets', function () {
    it("Given loggers Then must call _updateTargetProxyLoggers", function () {
      const instance = createInstance();

      // @ts-ignore
      spyOn(instance, '_updateTargetProxyLoggers');

      instance['properties'].loggers = {
        ____default____: {
          instance: jasmineUtils.spyOnClass(BaseProxyLogger),
          loggerName: '____default____'
        },
        u: {
          instance: jasmineUtils.spyOnClass(BaseProxyLogger),
          loggerName: 'u'
        },
        tg: {
          instance: jasmineUtils.spyOnClass(BaseProxyLogger),
          loggerName: 'tg'
        }
      };
      instance.removeAllTargets();
      expect(instance['_updateTargetProxyLoggers']).toHaveBeenCalledTimes(3);
      expect(instance['_updateTargetProxyLoggers']).toHaveBeenCalledWith('____default____', undefined);
      expect(instance['_updateTargetProxyLoggers']).toHaveBeenCalledWith('u', undefined);
      expect(instance['_updateTargetProxyLoggers']).toHaveBeenCalledWith('tg', undefined);
    });
  }); // #removeAllTargets

  describe('#_updateTargetProxyLoggers', function () {
    it("Given nil target Then must call _removeTargetProxyLoggers", function () {
      const instance = createInstance();

      // @ts-ignore
      spyOn(instance, '_removeTargetProxyLoggers');

      instance['_updateTargetProxyLoggers']('ik', null);
      expect(instance['_removeTargetProxyLoggers']).toHaveBeenCalledTimes(1);
      expect(instance['_removeTargetProxyLoggers']).toHaveBeenCalledWith('ik');
    });

    it("Given loggers Then must update target", function () {
      const instance = createInstance();

      // @ts-ignore
      spyOn(instance, '_removeTargetProxyLoggers');

      instance['properties'].proxies = {
        a: {
          instance: jasmineUtils.spyOnClass(BaseProxyLogger),
          targetLoggerName: 'po'
        },
        t: {
          instance: jasmineUtils.spyOnClass(BaseProxyLogger),
          targetLoggerName: 'cv'
        },
        cv: {
          instance: jasmineUtils.spyOnClass(BaseProxyLogger),
          targetLoggerName: 'av'
        }
      };
      const target = jasmineUtils.spyOnClass(BaseProxyLogger);
      target.metaData = {};

      instance['_updateTargetProxyLoggers']('cv', target);
      expect(instance['_removeTargetProxyLoggers']).not.toHaveBeenCalled();

      expect(instance['properties'].proxies['a'].instance.target).toBeUndefined();
      expect(instance['properties'].proxies['t'].instance.target).toEqual(target);
      expect(instance['properties'].proxies['cv'].instance.target).toEqual(target);
    });
  }); // #_updateTargetProxyLoggers

  describe('#_removeTargetProxyLoggers', function () {
    it("Given logger Then must set default target", function () {
      const instance = createInstance();

      const target = jasmineUtils.spyOnClass(BaseProxyLogger);
      target['ol'] = 'udj';
      Object.defineProperty(instance, 'defaultLoggerTarget', {
        value: target
      });

      instance['properties'].proxies = {
        a: {
          instance: jasmineUtils.spyOnClass(BaseProxyLogger),
          targetLoggerName: 'po'
        },
        t: {
          instance: jasmineUtils.spyOnClass(BaseProxyLogger),
          targetLoggerName: 'cv'
        },
        cv: {
          instance: jasmineUtils.spyOnClass(BaseProxyLogger),
          targetLoggerName: 'av'
        }
      };

      instance['properties'].proxies['a'].instance.target = {
        t: 45,
        log() {},
        isVerboseEnabled(): boolean { return false; },
        writeLog(options: targetLogger.WriteLogOptions): void {}
      } as TargetLogger;

      instance['properties'].proxies['t'].instance.target = {
        t: 20,
        log() {},
        isVerboseEnabled(): boolean { return false; },
        writeLog(options: targetLogger.WriteLogOptions): void {}
      } as TargetLogger;

      instance['properties'].proxies['cv'].instance.target = {
        t: 98,
        log() {},
        isVerboseEnabled(): boolean { return false; },
        writeLog(options: targetLogger.WriteLogOptions): void {}
      } as TargetLogger;

      instance['_removeTargetProxyLoggers']('cv');
      expect(instance['properties'].proxies['a'].instance.target).toBeDefined();
      expect(instance['properties'].proxies['cv'].instance.target).toBeDefined();
      expect(instance['properties'].proxies['t'].instance.target).toEqual(target);
      expect(instance['properties'].proxies['t'].targetLoggerName).toEqual('____default____');
    });
  }); // #_removeTargetProxyLoggers
});
