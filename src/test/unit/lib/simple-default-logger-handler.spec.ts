import {SimpleDefaultLoggerHandler} from '../../../lib/simple-default-logger-handler';
import {SimpleLoggerManagerV1} from '../../../lib/services/simple-logger-manager-v1';
import {jasmineUtils} from "zrim-test-bootstrap/dist/lib/utils";
import {targetLogger} from "../../../lib/target-logger";
import {BaseProxyLogger} from "../../../lib";

describe('#SimpleDefaultLoggerHandler', function () {
  function createInstance(): SimpleDefaultLoggerHandler {
    return new SimpleDefaultLoggerHandler();
  }

  describe('#defaultLoggerManager', function () {
    describe('#get', function () {
      it("Then must return the instance", function () {
        const instance = createInstance();

        expect(instance.defaultLoggerManager).toBe(instance.defaultLoggerManager);
        expect(instance.defaultLoggerManager).toEqual(jasmine.any(SimpleLoggerManagerV1));
      });
    }); // #get
  }); // #defaultLoggerManager

  describe('#_retrieveLoggerManager', function () {
    const _ = require('lodash');

    it("Given eu.zrimeverything.proxyLogger.defaultLoggerManager Then must return expected value", function () {
      const instance = createInstance();

      const defaultManager = {
        u: 10
      };
      Object.defineProperty(instance, 'defaultLoggerManager', {
        value: defaultManager
      });

      const expectedValue = {
        getVersion() { return ''; }
      };
      _.set(global, 'eu.zrimeverything.proxyLogger.defaultLoggerManager', expectedValue);
      _.set(global, 'jsZrimCore.defaultLoggerManager', 12);
      expect(instance['_retrieveLoggerManager']()).toBe(expectedValue);
      delete global['eu'];
      delete global['jsZrimCore'];
    });

    it("Given eu.zrimeverything.proxyLogger.defaultLoggerManager Then must return expected value", function () {
      const instance = createInstance();

      const defaultManager = {
        u: 10
      };
      Object.defineProperty(instance, 'defaultLoggerManager', {
        value: defaultManager
      });

      const expectedValue = {
        getVersion() { return ''; }
      };
      _.set(global, 'eu.zrimeverything.core.defaultLoggerManager', expectedValue);
      _.set(global, 'jsZrimCore.defaultLoggerManager', 12);
      expect(instance['_retrieveLoggerManager']()).toBe(expectedValue);
      delete global['eu'];
      delete global['jsZrimCore'];
    });

    it("Given eu.zrimeverything.proxyLogger.defaultLoggerManager Then must return expected value", function () {
      const instance = createInstance();

      const defaultManager = {
        u: 10
      };
      Object.defineProperty(instance, 'defaultLoggerManager', {
        value: defaultManager
      });

      const expectedValue = {
        getVersion() { return ''; }
      };
      _.set(global, 'jsZrimCore.defaultLoggerManager', expectedValue);
      expect(instance['_retrieveLoggerManager']()).toBe(expectedValue);
      delete global['jsZrimCore'];
    });

    it("Given no global value Then must return default one", function () {
      const instance = createInstance();

      const defaultManager = jasmineUtils.spyOnClass(SimpleLoggerManagerV1);
      Object.defineProperty(instance, 'defaultLoggerManager', {
        value: defaultManager
      });

      expect(instance['_retrieveLoggerManager']()).toEqual(defaultManager);
    });
  }); // #_retrieveLoggerManager

  describe('#_updateInternalLoggerManager', function () {
    it("Given same manager Then must do nothing", function () {
      const instance = createInstance();

      const manager = {
        p: 10,
        getVersion: jasmine.createSpy('getVersion')
      };
      // @ts-ignore
      spyOn(instance, '_retrieveLoggerManager').and.returnValue(manager);

      instance['properties'].loggerManager.instance = manager;

      instance['_updateInternalLoggerManager']();
      expect(instance['properties'].loggerManager.instance).toBe(manager);
      expect(instance['_retrieveLoggerManager']).toHaveBeenCalled();
      expect(manager.getVersion).not.toHaveBeenCalled();
    });

    it("Given no manager Then must throw error", function () {
      const instance = createInstance();

      // @ts-ignore
      spyOn(instance, '_retrieveLoggerManager');

      instance['properties'].loggerManager.instance = {
        getVersion() { return ''; }
      };

      expect(() => instance['_updateInternalLoggerManager']()).toThrow(jasmine.any(Error));
      expect(instance['_retrieveLoggerManager']).toHaveBeenCalled();
    });

    it("Given manager without getVersion method Then must throw error", function () {
      const instance = createInstance();

      const manager = {
        p: 10
      };
      // @ts-ignore
      spyOn(instance, '_retrieveLoggerManager').and.returnValue(manager);

      instance['properties'].loggerManager.instance = {
        getVersion() { return ''; }
      };

      expect(() => instance['_updateInternalLoggerManager']()).toThrow(jasmine.any(TypeError));
      expect(instance['_retrieveLoggerManager']).toHaveBeenCalled();
    });

    it("Given different manager and valid new one THen must keep new one", function () {
      const instance = createInstance();

      const manager = {
        p: 10,
        getVersion: jasmine.createSpy('getVersion').and.returnValue('77.22.33')
      };
      // @ts-ignore
      spyOn(instance, '_retrieveLoggerManager').and.returnValue(manager);

      // @ts-ignore
      spyOn(instance, '_extractMajorVersionNumber').and.returnValue(127);

      instance['properties'].loggerManager.instance = {
        getVersion() { return ''; }
      };

      instance['_updateInternalLoggerManager']();
      expect(instance['properties'].loggerManager.instance).toBe(manager);
      expect(instance['properties'].loggerManager.version).toBe(127);
      expect(instance['_retrieveLoggerManager']).toHaveBeenCalled();
      expect(manager.getVersion).toHaveBeenCalled();
      expect(instance['_extractMajorVersionNumber']).toHaveBeenCalledWith('77.22.33');
    });
  }); // #_updateInternalLoggerManager

  describe('#_extractMajorVersionNumber', function () {
    it("Given invalid version format Then must throw error", function () {
      const instance = createInstance();

      expect(() => instance['_extractMajorVersionNumber']('12')).toThrow(jasmine.any(TypeError));
    });

    it("Given valid version Then must return expected value", function () {
      const instance = createInstance();

      expect(instance['_extractMajorVersionNumber']('125.23.365')).toBe(125);
    });
  }); // #_extractMajorVersionNumber

  describe('#getLogger', function () {
    it("Given version not handled Then must throw error", function () {
      const instance = createInstance();

      // @ts-ignore
      spyOn(instance, '_updateInternalLoggerManager');

      instance['properties'].loggerManager.version = 7788;
      expect(() => instance.getLogger('114')).toThrow(jasmine.any(TypeError));
      expect(instance['_updateInternalLoggerManager']).toHaveBeenCalledTimes(1);
    });

    it("Given know version Then must return expected value", function () {
      const instance = createInstance();

      // @ts-ignore
      spyOn(instance, '_updateInternalLoggerManager');

      const expectedValue = {
        ol: 12
      };
      instance['_getLoggerV7788'] = jasmine.createSpy('_getLoggerV').and.returnValue(expectedValue);
      instance['properties'].loggerManager.instance = {
        getVersion() { return ''; }
      };
      instance['properties'].loggerManager.version = 7788;

      // @ts-ignore
      expect(instance.getLogger('lkiuj')).toBe(expectedValue);
      expect(instance['_getLoggerV7788']).toHaveBeenCalledTimes(1);
      expect(instance['_getLoggerV7788']).toHaveBeenCalledWith({
        loggerName: 'lkiuj',
        loggerManager: instance['properties'].loggerManager
      });
      expect(instance['_updateInternalLoggerManager']).toHaveBeenCalledTimes(1);
    });
  }); // #getLogger

  describe('#getTarget', function () {
    it("Given version not handled Then must throw error", function () {
      const instance = createInstance();

      // @ts-ignore
      spyOn(instance, '_updateInternalLoggerManager');

      instance['properties'].loggerManager.version = 7788;
      expect(() => instance.getTarget('114')).toThrow(jasmine.any(TypeError));
      expect(instance['_updateInternalLoggerManager']).toHaveBeenCalledTimes(1);
    });

    it("Given know version Then must return expected value", function () {
      const instance = createInstance();

      // @ts-ignore
      spyOn(instance, '_updateInternalLoggerManager');

      const expectedValue = {
        writeLog(options: targetLogger.WriteLogOptions): void {},
        isVerboseEnabled(): boolean { return false; }
      };
      instance['_getTargetV7788'] = jasmine.createSpy('_getTargetV').and.returnValue(expectedValue);
      instance['properties'].loggerManager.instance = {
        getVersion() { return ''; }
      };
      instance['properties'].loggerManager.version = 7788;

      expect(instance.getTarget('lkiuj')).toBe(expectedValue);
      expect(instance['_getTargetV7788']).toHaveBeenCalledTimes(1);
      expect(instance['_getTargetV7788']).toHaveBeenCalledWith({
        targetName: 'lkiuj',
        loggerManager: instance['properties'].loggerManager
      });
      expect(instance['_updateInternalLoggerManager']).toHaveBeenCalledTimes(1);
    });
  }); // #getTarget

  describe('#setTarget', function () {
    it("Given version not handled Then must throw error", function () {
      const instance = createInstance();

      // @ts-ignore
      spyOn(instance, '_updateInternalLoggerManager');

      instance['properties'].loggerManager.version = 7788;

      const target = {
        writeLog(options: targetLogger.WriteLogOptions): void {},
        isVerboseEnabled(): boolean { return false; }
      };
      expect(() => instance.setTarget('114', target)).toThrow(jasmine.any(TypeError));
      expect(instance['_updateInternalLoggerManager']).toHaveBeenCalledTimes(1);
    });

    it("Given know version Then must return call setter", function () {
      const instance = createInstance();

      // @ts-ignore
      spyOn(instance, '_updateInternalLoggerManager');


      instance['_setTargetV7788'] = jasmine.createSpy('_setTargetV');
      instance['properties'].loggerManager.instance = {
        getVersion() { return ''; }
      };
      instance['properties'].loggerManager.version = 7788;

      const target = {
        writeLog(options: targetLogger.WriteLogOptions): void {},
        isVerboseEnabled(): boolean { return false; }
      };
      instance.setTarget('lkiuj', target);
      expect(instance['_setTargetV7788']).toHaveBeenCalledTimes(1);
      expect(instance['_setTargetV7788']).toHaveBeenCalledWith({
        targetName: 'lkiuj',
        loggerManager: instance['properties'].loggerManager,
        target
      });
    });
  }); // #setLogger

  describe('#listLoggerNames', function () {
    it("Given version not handled Then must throw error", function () {
      const instance = createInstance();

      // @ts-ignore
      spyOn(instance, '_updateInternalLoggerManager');

      instance['properties'].loggerManager.version = 7788;
      expect(() => instance.listLoggerNames()).toThrow(jasmine.any(TypeError));
      expect(instance['_updateInternalLoggerManager']).toHaveBeenCalledTimes(1);
    });

    it("Given know version Then must return expected value", function () {
      const instance = createInstance();

      // @ts-ignore
      spyOn(instance, '_updateInternalLoggerManager');

      instance['_listLoggerNamesV7788'] = jasmine.createSpy('_listLoggerNamesV').and.returnValue(['klo', 'iks', 'cbc']);
      instance['properties'].loggerManager.instance = {
        getVersion() { return ''; }
      };
      instance['properties'].loggerManager.version = 7788;

      expect(instance.listLoggerNames()).toEqual(['klo', 'iks', 'cbc']);
      expect(instance['_listLoggerNamesV7788']).toHaveBeenCalledTimes(1);
      expect(instance['_listLoggerNamesV7788']).toHaveBeenCalledWith({
        loggerManager: instance['properties'].loggerManager
      });
      expect(instance['_updateInternalLoggerManager']).toHaveBeenCalledTimes(1);
    });
  }); // #listLoggerNames

  describe('#listTargetNames', function () {
    it("Given version not handled Then must throw error", function () {
      const instance = createInstance();

      // @ts-ignore
      spyOn(instance, '_updateInternalLoggerManager');

      instance['properties'].loggerManager.version = 7788;
      expect(() => instance.listTargetNames()).toThrow(jasmine.any(TypeError));
      expect(instance['_updateInternalLoggerManager']).toHaveBeenCalledTimes(1);
    });

    it("Given know version Then must return expected value", function () {
      const instance = createInstance();

      // @ts-ignore
      spyOn(instance, '_updateInternalLoggerManager');

      instance['_listTargetNamesV7788'] = jasmine.createSpy('_listTargetNamesV').and.returnValue(['klo', 'iks', 'cbc']);
      instance['properties'].loggerManager.instance = {
        getVersion() { return ''; }
      };
      instance['properties'].loggerManager.version = 7788;

      expect(instance.listTargetNames()).toEqual(['klo', 'iks', 'cbc']);
      expect(instance['_listTargetNamesV7788']).toHaveBeenCalledTimes(1);
      expect(instance['_listTargetNamesV7788']).toHaveBeenCalledWith({
        loggerManager: instance['properties'].loggerManager
      });
      expect(instance['_updateInternalLoggerManager']).toHaveBeenCalledTimes(1);
    });
  }); // #listTargetNames

  describe('#_getLoggerV1', function () {
    it("Then must return expected value", function () {
      const instance = createInstance();

      const loggerManager = {
        instance: jasmineUtils.spyOnClass(SimpleLoggerManagerV1),
        version: 11254
      };
      const logger = jasmineUtils.spyOnClass(BaseProxyLogger);
      loggerManager.instance.getLogger.and.returnValue(logger);
      expect(instance['_getLoggerV1']({
        loggerManager,
        loggerName: 'cnm'
      })).toEqual(logger);
      expect(loggerManager.instance.getLogger).toHaveBeenCalledWith('cnm');
    });
  }); // #_getLoggerV1

  describe('#_listLoggerNamesV1', function () {
    it("Then must return expected value", function () {
      const instance = createInstance();

      const loggerManager = {
        instance: jasmineUtils.spyOnClass(SimpleLoggerManagerV1),
        version: 11254
      };
      loggerManager.instance.listLoggerNames.and.returnValue(['ju', 'ooc']);
      expect(instance['_listLoggerNamesV1']({
        loggerManager
      })).toEqual(['ju', 'ooc']);
      expect(loggerManager.instance.listLoggerNames).toHaveBeenCalled();
    });
  }); // #_listLoggerNamesV1

  describe('#_listTargetNamesV1', function () {
    it("Then must return expected value", function () {
      const instance = createInstance();

      const loggerManager = {
        instance: jasmineUtils.spyOnClass(SimpleLoggerManagerV1),
        version: 11254
      };
      loggerManager.instance.listTargetLoggerNames.and.returnValue(['ju', 'ooc']);
      expect(instance['_listTargetNamesV1']({
        loggerManager
      })).toEqual(['ju', 'ooc']);
      expect(loggerManager.instance.listTargetLoggerNames).toHaveBeenCalled();
    });
  }); // #_listTargetNamesV1

  describe('#_getTargetV1', function () {
    it("Then must return expected value", function () {
      const instance = createInstance();

      const loggerManager = {
        instance: jasmineUtils.spyOnClass(SimpleLoggerManagerV1),
        version: 11254
      };
      const target = {
        writeLog(options: targetLogger.WriteLogOptions): void {},
        isVerboseEnabled(): boolean { return false; }
      };
      loggerManager.instance.getTargetLogger.and.returnValue(target);
      expect(instance['_getTargetV1']({
        loggerManager,
        targetName: 'cnm'
      })).toEqual(target);
      expect(loggerManager.instance.getTargetLogger).toHaveBeenCalledWith('cnm');
    });
  }); // #_getTargetV1

  describe('#_setTargetV1', function () {
    it("Then must return expected value", function () {
      const instance = createInstance();

      const loggerManager = {
        instance: jasmineUtils.spyOnClass(SimpleLoggerManagerV1),
        version: 11254
      };
      const target = {
        writeLog(options: targetLogger.WriteLogOptions): void {},
        isVerboseEnabled(): boolean { return false; }
      };
      instance['_setTargetV1']({
        loggerManager,
        targetName: 'cnm',
        target
      });
      expect(loggerManager.instance.setTargetLogger).toHaveBeenCalledWith('cnm', target);
    });
  }); // #_setTargetV1
});
