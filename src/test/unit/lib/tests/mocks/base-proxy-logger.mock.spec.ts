import {BaseProxyLogger} from '../../../../../lib/tests/mocks/base-proxy-logger.mock';

describe('#BaseProxyLogger', function () {
  function createInstance(): BaseProxyLogger {
    return new BaseProxyLogger();
  }

  ['alert', 'crit', 'debug', 'emerg', 'error', 'info', 'silly', 'warn', 'warning', 'notice'].forEach(name => {
    describe(`#${name}`, function () {
      it("Then must do nothing", function () {
        const instance = createInstance();

        instance[name]();
        expect(true).toBeTruthy();
      });
    });
  });

  ['writeLog', 'log'].forEach(name => {
    describe(`#${name}`, function () {
      it("Then must do nothing", function () {
        const instance = createInstance();

        instance[name]();
        expect(true).toBeTruthy();
      });
    });
  });

  describe('#serializeMetaData', function () {
    it("Then must return undefined", function () {
      const instance = createInstance();

      expect(instance.serializeMetaData({})).toBeUndefined();
    });
  }); // #serializeMetaData

  describe('#isVerboseEnabled', function () {
    it("Then must return false", function () {
      const instance = createInstance();

      expect(instance.isVerboseEnabled()).toBe(false);
    });
  }); // #isVerboseEnabled

  describe('#of', function () {
    it('Then must return new instance', function () {
      const instance = createInstance();

      expect(instance.of()).toEqual(jasmine.any(BaseProxyLogger));
    });
  }); // #of
}); // #BaseProxyLogger
